using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Visuals.Testers.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public MySplashScreen m_MySplashScreen;

            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get
                {
                    m_MySplashScreen = Create__Instance__(m_MySplashScreen);
                    return m_MySplashScreen;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_MySplashScreen))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_MySplashScreen);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public ReportTestPanel m_ReportTestPanel;

            public ReportTestPanel ReportTestPanel
            {
                [DebuggerHidden]
                get
                {
                    m_ReportTestPanel = Create__Instance__(m_ReportTestPanel);
                    return m_ReportTestPanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ReportTestPanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ReportTestPanel);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard m_Switchboard;

            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    m_Switchboard = Create__Instance__(m_Switchboard);
                    return m_Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Switchboard);
                }
            }
        }
    }
}
