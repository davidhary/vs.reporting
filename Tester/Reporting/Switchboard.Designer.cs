﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Testers
{
    [DesignerGenerated()]
    public partial class Switchboard
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {

                    // Free managed resources when explicitly called

                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }

            // Free shared unmanaged resources

            finally
            {

                // Invoke the base class dispose method
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _PanelComboBox = new ComboBox();
            __OpenButton = new Button();
            __OpenButton.Click += new EventHandler(OpenButton_Click);
            _AboutButton = new Button();
            _MessagesList = new Core.Forma.MessagesBox();
            _StatusLabel = new Label();
            __ExitButton = new Button();
            __ExitButton.Click += new EventHandler(ExitButton_Click);
            __CancelButton = new Button();
            __CancelButton.Click += new EventHandler(CallOffButton_Click);
            SuspendLayout();
            // 
            // _PanelComboBox
            // 
            _PanelComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _PanelComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _PanelComboBox.Font = new Font(Font, FontStyle.Bold);
            _PanelComboBox.Location = new Point(15, 246);
            _PanelComboBox.Name = "_PanelComboBox";
            _PanelComboBox.Size = new Size(244, 21);
            _PanelComboBox.TabIndex = 13;
            // 
            // _OpenButton
            // 
            __OpenButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __OpenButton.FlatStyle = FlatStyle.System;
            __OpenButton.Font = new Font(Font, FontStyle.Bold);
            __OpenButton.Location = new Point(260, 245);
            __OpenButton.Name = "__OpenButton";
            __OpenButton.Size = new Size(60, 23);
            __OpenButton.TabIndex = 12;
            __OpenButton.Text = "&Open";
            // 
            // _AboutButton
            // 
            _AboutButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _AboutButton.FlatStyle = FlatStyle.System;
            _AboutButton.Font = new Font(Font, FontStyle.Bold);
            _AboutButton.Location = new Point(136, 285);
            _AboutButton.Name = "_AboutButton";
            _AboutButton.Size = new Size(60, 23);
            _AboutButton.TabIndex = 11;
            _AboutButton.Text = "&About";
            // 
            // _MessagesList
            // 
            _MessagesList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _MessagesList.CausesValidation = false;
            _MessagesList.Location = new Point(13, 45);
            _MessagesList.Multiline = true;
            _MessagesList.Name = "_MessagesList";
            _MessagesList.PresetCount = 50;
            _MessagesList.ReadOnly = true;
            _MessagesList.ResetCount = 100;
            _MessagesList.ScrollBars = ScrollBars.Both;
            _MessagesList.Size = new Size(309, 176);
            _MessagesList.TabIndex = 10;
            // 
            // _StatusLabel
            // 
            _StatusLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _StatusLabel.FlatStyle = FlatStyle.System;
            _StatusLabel.Font = new Font(Font, FontStyle.Bold);
            _StatusLabel.Location = new Point(14, 5);
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new Size(305, 32);
            _StatusLabel.TabIndex = 8;
            _StatusLabel.Text = "Status: Ready";
            // 
            // _ExitButton
            // 
            __ExitButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __ExitButton.DialogResult = DialogResult.OK;
            __ExitButton.FlatStyle = FlatStyle.System;
            __ExitButton.Font = new Font(Font, FontStyle.Bold);
            __ExitButton.Location = new Point(242, 285);
            __ExitButton.Name = "__ExitButton";
            __ExitButton.Size = new Size(60, 23);
            __ExitButton.TabIndex = 7;
            __ExitButton.Text = "E&xit";
            // 
            // _CancelButton
            // 
            __CancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __CancelButton.DialogResult = DialogResult.Cancel;
            __CancelButton.FlatStyle = FlatStyle.System;
            __CancelButton.Font = new Font(Font, FontStyle.Bold);
            __CancelButton.Location = new Point(30, 285);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new Size(60, 23);
            __CancelButton.TabIndex = 9;
            __CancelButton.Text = "&Cancel";
            // 
            // Switchboard
            // 
            ClientSize = new Size(335, 325);
            Controls.Add(_PanelComboBox);
            Controls.Add(__OpenButton);
            Controls.Add(_AboutButton);
            Controls.Add(_MessagesList);
            Controls.Add(_StatusLabel);
            Controls.Add(__ExitButton);
            Controls.Add(__CancelButton);
            Name = "Switchboard";
            Text = "Switch Board";
            Closing += new System.ComponentModel.CancelEventHandler(Form_Closing);
            Load += new EventHandler(Form_Load);
            Activated += new EventHandler(Tester_Activated);
            ResumeLayout(false);
            PerformLayout();
        }

        private ComboBox _PanelComboBox;
        private Button __OpenButton;

        private Button _OpenButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenButton != null)
                {
                    __OpenButton.Click -= OpenButton_Click;
                }

                __OpenButton = value;
                if (__OpenButton != null)
                {
                    __OpenButton.Click += OpenButton_Click;
                }
            }
        }

        private Button _AboutButton;
        private Core.Forma.MessagesBox _MessagesList;
        private Label _StatusLabel;
        private Button __ExitButton;

        private Button _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButton_Click;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButton_Click;
                }
            }
        }

        private Button __CancelButton;

        private Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CallOffButton_Click;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CallOffButton_Click;
                }
            }
        }
    }
}