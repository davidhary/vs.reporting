/// <summary> A person. </summary>
/// <remarks> David, 2020-10-27. </remarks>

namespace isr.Visuals.Testers
{
    public class Person
    {
        public Person( string name, string city, string state )
        {
            this.Name = name;
            this.City = city;
            this.State = state;
        }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the city. </summary>
        /// <value> The city. </value>
        public string City { get; set; }

        /// <summary> Gets or sets the state. </summary>
        /// <value> The state. </value>
        public string State { get; set; }
    }
}
