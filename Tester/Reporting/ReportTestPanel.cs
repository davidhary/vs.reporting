using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Capsule.ExceptionExtensions;
using isr.Visuals.Reporting;

namespace isr.Visuals.Testers
{

    /// <summary> Uses a chart to test reporting. </summary>
    /// <remarks>
    /// David, 2014-02-15, Reported. <para>
    /// Launch this form by calling its Show or ShowDialog method from its default instance.
    /// </para><para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public partial class ReportTestPanel : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public ReportTestPanel() : base()
        {

            // instantiate objects that depend on resize.

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            this._MessagesList.CommenceUpdates();
            this.__TestButton.Name = "_TestButton";
            this.__ExitButton.Name = "_ExitButton";
            this.__CancelButton.Name = "_CancelButton";
            this.__ChartTabPage.Name = "_ChartTabPage";
        }


        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A message describing the status. </value>
        private string StatusMessage { get; set; } = string.Empty;

        /// <summary> Gets or sets the buffer for graphics data. </summary>
        /// <value> A buffer for graphics data. </value>
        private BufferedGraphics GraphicsBuffer { get; set; }

        /// <summary> Gets or sets the chart pane. </summary>
        /// <value> The chart pane. </value>
        private ChartPane ChartPane { get; set; }

        /// <summary> Creates the chart. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void CreateChart()
        {
            this.GraphicsBuffer = new BufferedGraphics( this.ClientRectangle.Width, this.ClientRectangle.Height );
            this.ChartPane = new ChartPane();
            var rec = new Rectangle( this._MessagesList.Location, this._MessagesList.Size );
            this.ChartPane.CreateSampleOne( rec );
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {

            // set the combo box
            this._ReportOptionsComboBox.Items.Clear();
            _ = this._ReportOptionsComboBox.Items.Add( "select a report option:" );
            _ = this._ReportOptionsComboBox.Items.Add( "Report coded in the program" );
            _ = this._ReportOptionsComboBox.Items.Add( "Report coded in the program with auto paging" );
            _ = this._ReportOptionsComboBox.Items.Add( "Report from object data." );
            _ = this._ReportOptionsComboBox.Items.Add( "Multi-Section report." );
            _ = this._ReportOptionsComboBox.Items.Add( "Report from SQL data set." );
            _ = this._ReportOptionsComboBox.Items.Add( "Report from SQL and object data." );
            this._ReportOptionsComboBox.SelectedIndex = 0;
            this.CreateChart();
        }

        /// <summary> Terminates and disposes of class-level objects. </summary>
        /// <remarks> Called from the form Closing method. </remarks>
        private void TerminateObjects()
        {
            this._MessagesList.SuspendUpdatesReleaseIndicators();
            if ( this.GraphicsBuffer is object )
            {
                this.GraphicsBuffer.Dispose();
                this.GraphicsBuffer = null;
            }

            if ( this.ChartPane is object )
            {
                this.ChartPane.Dispose();
                this.ChartPane = null;
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      <see cref="System.EventArgs" /> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // set the form caption
                this.Text = My.MyApplication.Appliance.Info.BuildDefaultCaption( ": REPORT TEST PANEL" );

                // set tool tips
                this.InitializeUserInterface();

                // center the form
                this.CenterToScreen();
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {

                // Turn off the form hourglass cursor
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Paints the chart. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="parent"> The parent. </param>
        /// <param name="e">      The <see cref="System.Windows.Forms.PaintEventArgs" /> instance
        /// containing the event data. </param>
        private void PaintChart( Control parent, PaintEventArgs e )
        {
            if ( !this.IsDisposed && parent is object )
            {
                if ( this.GraphicsBuffer.CanDoubleBuffer() )
                {
                    using ( var brush = new SolidBrush( SystemColors.Window ) )
                    {
                        // Fill in Background (for efficiency only the area that has been clipped)
                        // Me.GraphicsBuffer.GraphicsDevice.FillRectangle(brush, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height)
                        this.GraphicsBuffer.GraphicsDevice.FillRectangle( brush, e.ClipRectangle );
                    }

                    using ( var brush = new SolidBrush( Color.Gray ) )
                    {
                        // clear the client area
                        this.GraphicsBuffer.GraphicsDevice.FillRectangle( brush, parent.ClientRectangle );
                    }

                    // Do our drawing using Me._graphicsBuffer.g instead e.Graphics
                    this.ChartPane.Draw( this.GraphicsBuffer.GraphicsDevice );

                    // Render to the control.
                    this.GraphicsBuffer.Render( e.Graphics );
                }
                else
                {
                    // if double buffer is not available, draw to e.Graphics

                    // clear
                    using ( var brush = new SolidBrush( Color.Gray ) )
                    {
                        e.Graphics.FillRectangle( brush, parent.ClientRectangle );
                    }

                    // draw
                    this.ChartPane.Draw( e.Graphics );
                }
            }
        }

        /// <summary> Handles the Paint event of the _ChartTabPage control. Updates the chart. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Windows.Forms.PaintEventArgs" /> instance
        /// containing the event data. </param>
        private void ChartTabPage_Paint( object sender, PaintEventArgs e )
        {
            if ( !this.IsDisposed && sender is object )
            {
                this.PaintChart( ( Control ) sender, e );
            }
        }

        /// <summary> Resizes the chart. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="parent"> The parent. </param>
        private void ResizeChart( Control parent )
        {
            if ( !this.IsDisposed && parent is object )
            {
                if ( this.GraphicsBuffer is object )
                {
                    _ = this.GraphicsBuffer.CreateDoubleBuffer( parent.ClientRectangle.Width, parent.ClientRectangle.Height );
                }

                if ( this.ChartPane is object )
                {
                    this.ChartPane.SetSize( parent.ClientRectangle );
                }

                parent.Invalidate();
            }
        }

        /// <summary> Handles the Resize event of the _ChartTabPage control. Resizes the chart. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void ChartTabPage_Resize( object sender, EventArgs e )
        {
            if ( !this.IsDisposed && sender is object )
            {
                this.ResizeChart( ( Control ) sender );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Closes the form and exits the application. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExitButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._StatusLabel.Text = $"{DateTime.Now.Ticks} {CommandLineInfo.DevicesEnabled.GetValueOrDefault( true )}";
                this.Close();
            }
            catch ( Exception ex )
            {

                // report failure 
                this.StatusMessage = $"Failed closing {Environment.NewLine}{ex.ToFullBlownString()}";
                _ = MessageBox.Show( this.StatusMessage, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary> Call off button click. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Event information. </param>
        private void CallOffButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Tests button click. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Event information. </param>
        private void TestButton_Click( object sender, EventArgs e )
        {

            // check if a report option was selected
            if ( this._ReportOptionsComboBox.SelectedIndex > 0 )
            {

                // show report option selected
                _ = this._MessagesList.AddMessage( $"Generating report: {this._ReportOptionsComboBox.Text}" );
                switch ( this._ReportOptionsComboBox.SelectedIndex )
                {
                    case 0:
                        {
                            break;
                        }

                    case 1:
                        {
                            this.DoReportFromCode();
                            break;
                        }

                    case 2:
                        {
                            this.DoReportAutoPaging();
                            break;
                        }

                    case 3:
                        {
                            DoReportFromObjectArrayList();
                            break;
                        }

                    case 4:
                        {
                            this.DoMultiPartReport();
                            break;
                        }

                    case 5:
                        {
                            DoReportFromDataSet();
                            break;
                        }

                    case 6:
                        {
                            this.DoReportFromMultiPartReportDataSources();
                            break;
                        }

                    default:
                        {
                            Debug.Assert( !Debugger.IsAttached, "Unhandled report option" );
                            break;
                        }
                }
            }
        }

        #endregion

        #region " REPORT FROM CODE "

        /// <summary> The with events. </summary>
        private ReportDocument _CodedReport;

        private ReportDocument CodedReport
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._CodedReport;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._CodedReport != null )
                {

                    /// <summary>
                    /// This event is raised for each page immediately after the header for the page has been
                    /// printed. The cursor is on the first line of the report body.
                    /// </summary>
                    /// <remarks> Use this method to print the body of the report. </remarks>
                    /// <param name="sender"> <see cref="object" /> instance of this
                    /// <see cref="Form" /> </param>
                    /// <param name="e">      Report page event information. </param>
                    this._CodedReport.PrintPageBodyStarted -= this.CodedReport_PrintPageBodyStart;

                    /// <summary> Coded report print page. </summary>
                    /// <remarks> David, 2020-10-27. </remarks>
                    /// <param name="sender"> <see cref="object" /> instance of this
                    /// <see cref="Form" /> </param>
                    /// <param name="e">      Print page event information. </param>
                    this._CodedReport.PrintPage -= this.CodedReport_PrintPage;
                }

                this._CodedReport = value;
                if ( this._CodedReport != null )
                {
                    this._CodedReport.PrintPageBodyStarted += this.CodedReport_PrintPageBodyStart;
                    this._CodedReport.PrintPage += this.CodedReport_PrintPage;
                }
            }
        }

        /// <summary> Executes the report from code operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void DoReportFromCode()
        {
            this.CodedReport = new ReportDocument();
            Heading heading;
            heading = this.CodedReport.AddHeading( DateTimeOffset.Now.ToString( System.Globalization.CultureInfo.CurrentCulture ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            heading = this.CodedReport.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Right;
            heading = this.CodedReport.AddHeading( "Report from code (program generated)", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = this.CodedReport.AddHeading( "Page {0} of {1}", HeadingType.SupFooter );
            heading.Appearance.Justification = LineJustification.Right;
            heading.IsPage = true;
            heading.IsPageCount = true;
            heading.Visible = true;
            heading = this.CodedReport.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            this.CodedReport.Font = new Font( "Ariel", 10f );

            // we have two pages
            this.CodedReport.PageCount = 2;

            // print preview
            this.CodedReport.Print( true );
        }

        private void CodedReport_PrintPageBodyStart( object sender, ReportPageEventArgs e )
        {
            ReportDocument reportDoc = ( ReportDocument ) sender;
            if ( e.PageNumber == 1 )
            {
                e.WriteLine( "Welcome to our report." );
                e.WriteLine();
                e.Write( "We can print some text, and " );
                e.WriteLine( "then put some more text on the same line." );
                e.WriteLine( "This works much like writing to the console." );
                e.WriteLine();
                e.WriteLine();
                e.Write( "We can left-justify", LineJustification.Left );
                e.Write( "We can also center text", LineJustification.Centered );
                e.WriteLine( "And we can right-justify", LineJustification.Right );
                e.WriteLine();
                e.WriteLine();
                e.WriteLine( "Much simpler than doing this all by hand." );
                e.HasMorePages = reportDoc.PageCount > e.PageNumber;
            }
            else
            {
                e.WriteLine( "This is page 2." );
                e.WriteLine( "Just to show that we handle multiple pages." );

                // add printing of graph
                this.ChartPane.Print( e.Graphics, e.PageSettings.Bounds ); // New RectangleF(72, 288, 72, 288))

                // Me._chartPane.Print(e.Graphics, e.PageSettings.Bounds, New Rectangle(72, 288, 72, 288))

                e.HasMorePages = reportDoc.PageCount > e.PageNumber;
            }
        }

        private void CodedReport_PrintPage( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
        }

        #endregion

        #region " REPORT WITH AUTO PAGING "

        private ReportDocument _AutoPageReport;

        /// <summary>   Gets or sets the automatic page report. </summary>
        /// <value> The automatic page report. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private ReportDocument AutoPageReport
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._AutoPageReport;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._AutoPageReport != null )
                {
                    this._AutoPageReport.PrintPageBodyStarted -= this.AutoPageReport_PrintPageBodyStart;
                }

                this._AutoPageReport = value;
                if ( this._AutoPageReport != null )
                {
                    this._AutoPageReport.PrintPageBodyStarted += this.AutoPageReport_PrintPageBodyStart;
                }
            }
        }

        /// <summary> The consecutive line number. </summary>
        private int _ConsecutiveLineNumber;

        /// <summary> The lines to print. </summary>
        private const int _LinesToPrint = 200;

        /// <summary> Executes the report automatic paging operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void DoReportAutoPaging()
        {
            using var autoPageReport = new ReportDocument();
            Heading heading;
            heading = autoPageReport.AddHeading( DateTimeOffset.Now.ToString( "F" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            heading = autoPageReport.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Right;
            heading = autoPageReport.AddHeading( "Program generated report with auto paging", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = autoPageReport.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            autoPageReport.Font = new Font( "Ariel", 11f );
            this._ConsecutiveLineNumber = 0;

            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = autoPageReport,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        /// <summary> Automatic page report print page body start. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Report page event information. </param>
        private void AutoPageReport_PrintPageBodyStart( object sender, ReportPageEventArgs e )
        {
            while ( this._ConsecutiveLineNumber < _LinesToPrint )
            {
                // increment and print line number
                this._ConsecutiveLineNumber += 1;
                e.WriteLine( $"Consecutive Line Number: {this._ConsecutiveLineNumber:0000}" );

                // make sure we don't run off the end of the page
                if ( e.EndOfPage() )
                    break;
            }

            // indicate whether we have more data to display
            if ( this._ConsecutiveLineNumber < _LinesToPrint )
            {
                e.HasMorePages = true;
            }
        }

        #endregion

        #region " REPORT FROM DATA SET "

        /// <summary> Executes the report from data set operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private static void DoReportFromDataSet()
        {

            // load employees data from pubs
            string dbConn = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI";
            string query = "SELECT emp_id,FNAME,LNAME FROM employee";
            using var da = new System.Data.SqlClient.SqlDataAdapter( query, dbConn );
            using var ds = new DataSet {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            _ = da.Fill( ds );
            // initialize report
            using var report = new ReportDocument();
            Heading heading;
            heading = report.AddHeading( DateTimeOffset.Now.ToString( "G" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            heading = report.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Right;
            heading = report.AddHeading( "Pubs Employees", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = report.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            report.Font = new Font( "Ariel", 10f );

            // set the data source, using auto discover to find the columns
            report.AutoDiscover = true;
            report.DataSource = ds;

            // override the column names to be more human readable
            report.Columns[0].Name = "Employee id";
            report.Columns[1].Name = "First name";
            report.Columns[2].Name = "Last name";

            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = report,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        #endregion

        #region " REPORT FROM OBJECTS "

        /// <summary> Executes the report from object array list operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private static void DoReportFromObjectArrayList()
        {

            // load some data
            var data = new ArrayList() { new Person( "John", "Bloomington", "MN" ), new Person( "Mary", "Bloomington", "IL" ), new Person( "Aaron", "Minneapolis", "MN" ), new Person( "Ben", "San Francisco", "CA" ) };

            // initialize report
            using var report = new ReportDocument();
            Heading heading;
            heading = report.AddHeading( DateTimeOffset.Now.ToString( "G" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            heading = report.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Right;
            heading = report.AddHeading( "Report from Person Objects", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = report.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            report.Font = new Font( "Ariel", 10f );

            // set the data source, using auto discover to find the columns
            report.AutoDiscover = true;
            report.DataSource = data;

            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = report,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        #endregion

        #region " REPORT WITH MULTIPLE SECTIONS "

        /// <summary> The with events. </summary>
        private ReportDocument _MultiPartReport;

        private ReportDocument MultiPartReport
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MultiPartReport;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MultiPartReport != null )
                {
                    this._MultiPartReport.PrintPageBodyStarted -= this.MultiPartReport_PrintPageBodyStart;

                    /// <summary> Multi part report end. </summary>
                    /// <remarks> David, 2020-10-27. </remarks>
                    /// <param name="sender"> <see cref="object" /> instance of this
                    /// <see cref="Form" /> </param>
                    /// <param name="e">      Report page event information. </param>
                    this._MultiPartReport.ReportEnded -= this.MultiPartReport_ReportEnd;
                }

                this._MultiPartReport = value;
                if ( this._MultiPartReport != null )
                {
                    this._MultiPartReport.PrintPageBodyStarted += this.MultiPartReport_PrintPageBodyStart;
                    this._MultiPartReport.ReportEnded += this.MultiPartReport_ReportEnd;
                }
            }
        }

        /// <summary> List of object arrays. </summary>
        private readonly ArrayList _ObjectArrayList = new ArrayList();

        /// <summary> Executes the multi part report operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void DoMultiPartReport()
        {

            // load some data into a collection of objects
            _ = this._ObjectArrayList.Add( new Person( "John", "Bloomington", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Mary", "Bloomington", "IL" ) );
            _ = this._ObjectArrayList.Add( new Person( "Aaron", "Minneapolis", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Ben", "San Francisco", "CA" ) );

            // initialize report to report against DataSet
            using var multiPartReport = new ReportDocument();
            this.MultiPartReport = multiPartReport;
            Heading heading;
            heading = multiPartReport.AddHeading( DateTimeOffset.Now.ToString( "G" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            heading = multiPartReport.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Right;
            heading = multiPartReport.AddHeading( "Multi-Section Report", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = multiPartReport.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            multiPartReport.Font = new Font( "Ariel", 10f );

            // set the data source, using auto discover to find the columns
            multiPartReport.AutoDiscover = false;

            // start with the manual portion of the report section 1.
            multiPartReport.Section = 1;


            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = multiPartReport,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        /// <summary> Executes the report from multi part report data sources operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void DoReportFromMultiPartReportDataSources()
        {

            // load authors data from pubs
            string dbConn = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI";
            using var da = new System.Data.SqlClient.SqlDataAdapter( "SELECT emp_id,FNAME,LNAME FROM employee", dbConn );
            using var ds = new DataSet {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            _ = da.Fill( ds );
            // load some data into a collection of objects
            _ = this._ObjectArrayList.Add( new Person( "John", "Bloomington", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Mary", "Bloomington", "IL" ) );
            _ = this._ObjectArrayList.Add( new Person( "Aaron", "Minneapolis", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Ben", "San Francisco", "CA" ) );

            // initialize report to report against DataSet
            using var multiPartReport = new ReportDocument();
            this.MultiPartReport = multiPartReport;
            Heading heading;
            heading = multiPartReport.AddHeading( DateTimeOffset.Now.ToString( "G" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            this._SupHeaderRight = multiPartReport.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            this._SupHeaderRight.Appearance.Justification = LineJustification.Right;
            heading = multiPartReport.AddHeading( "Multi-Part Report", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = multiPartReport.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            multiPartReport.Font = new Font( "Ariel", 10f );

            // set the data source, using auto discover to find the columns
            multiPartReport.AutoDiscover = true;
            multiPartReport.DataSource = ds;

            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = multiPartReport,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        /// <summary>
        /// This event is raised for each page immediately after the header for the page has been
        /// printed. The cursor is on the first line of the report body.
        /// </summary>
        /// <remarks> Use this method to print the body of the report. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Report page event information. </param>
        private void MultiPartReport_PrintPageBodyStart( object sender, ReportPageEventArgs e )
        {
            if ( this.MultiPartReport.Section == 1 )
            {

                // if manual portion, do the manual part.
                if ( e.PageNumber == 1 )
                {
                    e.WriteLine( "Welcome to our report." );
                    e.WriteLine();
                    e.Write( "We can print some text, and " );
                    e.WriteLine( "then put some more text on the same line." );
                    e.WriteLine( "This works much like writing to the console." );
                    e.WriteLine();
                    e.WriteLine();
                    e.Write( "We can left-justify", LineJustification.Left );
                    e.Write( "We can also center text", LineJustification.Centered );
                    e.WriteLine( "And we can right-justify", LineJustification.Right );
                    e.WriteLine();
                    e.WriteLine();
                    e.WriteLine( "Much simpler than doing this all by hand." );
                    e.HasMorePages = true;
                }
                else
                {
                    e.WriteLine( "This is page 2." );
                    e.WriteLine( "Just to show that we handle multiple pages." );
                    e.HasMorePages = false;
                }
            }
        }

        private void MultiPartReport_ReportEnd( object sender, ReportPageEventArgs e )
        {

            // if we just reported the DataSet we don't want the 
            // report to end, so override to also report
            // the objects
            if ( this.MultiPartReport.DataSource is DataSet )
            {

                // indicate there ARE more pages
                e.HasMorePages = true;

                // DateTimeOffset.Now set the data source to our collection
                this.MultiPartReport.DataSource = this._ObjectArrayList;

                // set the data source, using auto discover to find the columns
                this.MultiPartReport.AutoDiscover = true;

                // change the subtitle to indicate the new type of data
                this._SupHeaderRight.Appearance.Text = "Person Objects";

                // turn on the second section of the report
                this.MultiPartReport.Section = 2;
            }
        }

        #endregion

        #region " REPORT WITH MULTIPLE DATA SETS "

        /// <summary> The with events. </summary>
        private ReportDocument _MultiDataSetsReport;

        private ReportDocument MultiDataSetsReport
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MultiDataSetsReport;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MultiDataSetsReport != null )
                {
                    this._MultiDataSetsReport.ReportEnded -= this.MultiDataSetsReport_ReportEnd;
                }

                this._MultiDataSetsReport = value;
                if ( this._MultiDataSetsReport != null )
                {
                    this._MultiDataSetsReport.ReportEnded += this.MultiDataSetsReport_ReportEnd;
                }
            }
        }

        /// <summary> The sup header right. </summary>
        private Heading _SupHeaderRight;

        /// <summary> Executes the multi data report operation. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void DoMultiDataReport()
        {

            // load authors data from pubs
            string dbConn = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI";
            using var da = new System.Data.SqlClient.SqlDataAdapter( "SELECT emp_id,FNAME,LNAME FROM employee", dbConn );
            using var ds = new DataSet {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            _ = da.Fill( ds );

            // load some data into a collection of objects
            _ = this._ObjectArrayList.Add( new Person( "John", "Bloomington", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Mary", "Bloomington", "IL" ) );
            _ = this._ObjectArrayList.Add( new Person( "Aaron", "Minneapolis", "MN" ) );
            _ = this._ObjectArrayList.Add( new Person( "Ben", "San Francisco", "CA" ) );

            // initialize report to report against DataSet
            using var multiDataSetsReport = new ReportDocument();
            this.MultiDataSetsReport = multiDataSetsReport;
            Heading heading;
            heading = multiDataSetsReport.AddHeading( DateTimeOffset.Now.ToString( "G" ), HeadingType.SupHeader );
            heading.Appearance.Justification = LineJustification.Left;
            this._SupHeaderRight = multiDataSetsReport.AddHeading( "ISR Report Generator", HeadingType.SupHeader );
            this._SupHeaderRight.Appearance.Justification = LineJustification.Right;
            heading = multiDataSetsReport.AddHeading( "Report with multiple data sets", HeadingType.Header );
            heading.Appearance.Font = new Font( heading.Appearance.Font, FontStyle.Bold );
            heading = multiDataSetsReport.AddHeading( "(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer );
            heading.Appearance.Justification = LineJustification.Left;
            multiDataSetsReport.Font = new Font( "Ariel", 10f );

            // set the data source, using auto discover to find the columns
            multiDataSetsReport.AutoDiscover = true;
            multiDataSetsReport.DataSource = ds;

            // display the report
            using var dlg = new PrintPreviewDialog {
                Document = this.MultiPartReport,
                WindowState = FormWindowState.Maximized
            };
            _ = dlg.ShowDialog();
        }

        /// <summary> Multi data sets report end. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object" /> instance of this
        /// <see cref="System.Windows.Forms.Form" /> </param>
        /// <param name="e">      Report page event information. </param>
        private void MultiDataSetsReport_ReportEnd( object sender, ReportPageEventArgs e )
        {

            // if we just reported the DataSet we don't want the 
            // report to end, so override to also report
            // the objects
            if ( this.MultiDataSetsReport.DataSource is DataSet )
            {

                // indicate there ARE more pages
                e.HasMorePages = true;

                // DateTimeOffset.Now set the data source to our collection
                this.MultiPartReport.DataSource = this._ObjectArrayList;

                // set the data source, using auto discover to find the columns
                this.MultiPartReport.AutoDiscover = true;

                // change the subtitle to indicate the new type of data
                this._SupHeaderRight.Appearance.Text = "Person Objects";
            }
        }

        #endregion

    }
}
