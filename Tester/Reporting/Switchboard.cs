using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using isr.Core.Capsule.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Testers
{

    /// <summary> Selects a test panel. </summary>
    /// <remarks>
    /// Launch this form by calling its Show or ShowDialog method from its default instance.  <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-02-15, Documented. </para>
    /// </remarks>
    public partial class Switchboard : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public Switchboard() : base()
        {

            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            this._MessagesList.CommenceUpdates();
            this.__OpenButton.Name = "_OpenButton";
            this.__ExitButton.Name = "_ExitButton";
            this.__CancelButton.Name = "_CancelButton";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A message describing the status. </value>
        private string StatusMessage { get; set; } = string.Empty;

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary> Enumerates the available test panels. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private enum TestPanel
        {

            /// <summary> An enum constant representing the none option. </summary>
            [System.ComponentModel.Description( "Not selected" )]
            None,

            /// <summary> An enum constant representing the reports option. </summary>
            [System.ComponentModel.Description( "Reports" )]
            Reports
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {
            this._PanelComboBox.Items.Clear();
            // Me.panelComboBox.DataSource = [Enum].GetNames(GetType(TestPanel))
            this._PanelComboBox.DataSource = Core.EnumExtensions.EnumExtensionsMethods.ValueDescriptionPairs( typeof( TestPanel ) ).ToList();
            this._PanelComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._PanelComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs before the form is closed. </summary>
        /// <remarks>
        /// Use this method to optionally cancel the closing of the form. Because the form is not yet
        /// closed at this point, this is also the best place to serialize a form's visible properties,
        /// such as size and location. Finally, dispose of any form level objects especially those that
        /// might needs access to the form and thus should not be terminated after the form closed.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
        private void Form_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {

            // disable the timer if any
            // actionTimer.Enabled = False
            Application.DoEvents();

            // set module objects that reference other objects to Nothing

            this.Cursor = Cursors.WaitCursor;
            try
            {
                // terminate form-level objects
                // Me.terminateObjects()
                this._MessagesList.SuspendUpdatesReleaseIndicators();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // instantiate form objects
                // Me.instantiateObjects()

                // set the form caption
                this.Text = My.MyApplication.Appliance.Info.BuildDefaultCaption( ": SWITCHBOARD" );

                // set tool tips
                this.InitializeUserInterface();

                // center the form
                this.CenterToScreen();
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Tester activated. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void Tester_Activated( object sender, EventArgs e )
        {
            _ = this._MessagesList.AddMessage( "Activated" );
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Closes the form and exits the application. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExitButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._StatusLabel.Text = $"{DateTime.Now.Ticks} {CommandLineInfo.DevicesEnabled.GetValueOrDefault( true )}";
                this.Close();
            }
            catch ( Exception ex )
            {

                // report failure 
                this.StatusMessage = $"Failed closing {Environment.NewLine}{ex.ToFullBlownString()}";
                _ = MessageBox.Show( this.StatusMessage, this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary> Call off button click. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void CallOffButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Opens the selected form. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void OpenButton_Click( object sender, EventArgs e )
        {
            KeyValuePair<Enum, string> selectedKeyValuePair;
            selectedKeyValuePair = ( KeyValuePair<Enum, string> ) this._PanelComboBox.SelectedItem;
            switch ( ( TestPanel ) Conversions.ToInteger( selectedKeyValuePair.Key ) )
            {
                case TestPanel.Reports:
                    {
                        using var r = new ReportTestPanel();
                        _ = r.ShowDialog();

                        break;
                    }
            }
        }

        #endregion

    }
}
