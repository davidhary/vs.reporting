Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Visuals + &H4

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Reporting Tester"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Reporting Tester"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Reporting.Library.Tester"

    End Class

End Namespace

