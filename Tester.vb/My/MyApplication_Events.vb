Imports isr.Visuals.Testers.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Builds the default caption. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> The caption. </returns>
        Friend Function BuildDefaultCaption() As String
            Dim suffix As New System.Text.StringBuilder
            suffix.Append(" ")
            Return isr.Core.ApplicationInfo.BuildApplicationDescriptionCaption(suffix.ToString)
        End Function

        ''' <summary> Destroys objects for this project. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        Friend Sub Destroy()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Function TryinitializeKnownState() As Boolean

            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                If My.MyApplication.InDesignMode Then
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Design Mode.")
                Else
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Runtime Mode.")
                End If

                ' Apply command line results.
                If CommandLineInfo.DevicesEnabled.HasValue Then
                    Me.SplashTraceEvent(TraceEventType.Information, "{0} use of devices from command line",
                                        If(CommandLineInfo.DevicesEnabled.Value, "Enabled", "Disabling"))
                    My.Settings.DevicesEnabled = CommandLineInfo.DevicesEnabled.Value
                End If

                Return True

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.SplashTraceEvent(TraceEventType.Error, "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString)
                Try
                    Me.Destroy()
                Finally
                End Try
                Return False
            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        Private Sub ProcessShutDown()

            My.Application.SaveMySettingsOnExit = True
            If My.Application.SaveMySettingsOnExit Then
                ' Save library settings here
            End If
        End Sub

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)
            If Not e.Cancel Then
                Me.SplashTraceEvent(TraceEventType.Verbose, "Using splash panel.")
                Me.SplashTraceEvent(TraceEventType.Verbose, "Parsing command line")
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine)
            End If
        End Sub

    End Class

End Namespace

