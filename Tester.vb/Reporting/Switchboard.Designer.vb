<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Switchboard

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._PanelComboBox = New System.Windows.Forms.ComboBox
        Me._OpenButton = New System.Windows.Forms.Button
        Me._AboutButton = New System.Windows.Forms.Button
        Me._MessagesList = New isr.Core.Forma.MessagesBox
        Me._StatusLabel = New System.Windows.Forms.Label
        Me._ExitButton = New System.Windows.Forms.Button
        Me._CancelButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        '_PanelComboBox
        '
        Me._PanelComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                          System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._PanelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PanelComboBox.Font = New Font(Me.Font, FontStyle.Bold)
        Me._PanelComboBox.Location = New System.Drawing.Point(15, 246)
        Me._PanelComboBox.Name = "_PanelComboBox"
        Me._PanelComboBox.Size = New System.Drawing.Size(244, 21)
        Me._PanelComboBox.TabIndex = 13
        '
        '_OpenButton
        '
        Me._OpenButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._OpenButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._OpenButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._OpenButton.Location = New System.Drawing.Point(260, 245)
        Me._OpenButton.Name = "_OpenButton"
        Me._OpenButton.Size = New System.Drawing.Size(60, 23)
        Me._OpenButton.TabIndex = 12
        Me._OpenButton.Text = "&Open"
        '
        '_AboutButton
        '
        Me._AboutButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._AboutButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AboutButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._AboutButton.Location = New System.Drawing.Point(136, 285)
        Me._AboutButton.Name = "_AboutButton"
        Me._AboutButton.Size = New System.Drawing.Size(60, 23)
        Me._AboutButton.TabIndex = 11
        Me._AboutButton.Text = "&About"
        '
        '_MessagesList
        '
        Me._MessagesList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                          System.Windows.Forms.AnchorStyles.Left) Or
                                      System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MessagesList.CausesValidation = False
        Me._MessagesList.Location = New System.Drawing.Point(13, 45)
        Me._MessagesList.Multiline = True
        Me._MessagesList.Name = "_MessagesList"
        Me._MessagesList.PresetCount = 50
        Me._MessagesList.ReadOnly = True
        Me._MessagesList.ResetCount = 100
        Me._MessagesList.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesList.Size = New System.Drawing.Size(309, 176)
        Me._MessagesList.TabIndex = 10
        '
        '_StatusLabel
        '
        Me._StatusLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StatusLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._StatusLabel.Font = New Font(Me.Font, FontStyle.Bold)
        Me._StatusLabel.Location = New System.Drawing.Point(14, 5)
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(305, 32)
        Me._StatusLabel.TabIndex = 8
        Me._StatusLabel.Text = "Status: Ready"
        '
        '_ExitButton
        '
        Me._ExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ExitButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._ExitButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._ExitButton.Location = New System.Drawing.Point(242, 285)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(60, 23)
        Me._ExitButton.TabIndex = 7
        Me._ExitButton.Text = "E&xit"
        '
        '_CancelButton
        '
        Me._CancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._CancelButton.Location = New System.Drawing.Point(30, 285)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(60, 23)
        Me._CancelButton.TabIndex = 9
        Me._CancelButton.Text = "&Cancel"
        '
        'Switchboard
        '
        Me.ClientSize = New System.Drawing.Size(335, 325)
        Me.Controls.Add(Me._PanelComboBox)
        Me.Controls.Add(Me._OpenButton)
        Me.Controls.Add(Me._AboutButton)
        Me.Controls.Add(Me._MessagesList)
        Me.Controls.Add(Me._StatusLabel)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._CancelButton)
        Me.Name = "Switchboard"
        Me.Text = "Switch Board"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _PanelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _OpenButton As System.Windows.Forms.Button
    Private WithEvents _AboutButton As System.Windows.Forms.Button
    Private WithEvents _MessagesList As isr.Core.Forma.MessagesBox
    Private WithEvents _StatusLabel As System.Windows.Forms.Label
    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _CancelButton As System.Windows.Forms.Button
End Class
