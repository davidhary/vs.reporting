''' <summary> A person. </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public Class Person

    Public Sub New(ByVal name As String, ByVal city As String, ByVal state As String)
        Me.Name = name
        Me.City = city
        Me.State = state
    End Sub

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property Name() As String

    ''' <summary> Gets or sets the city. </summary>
    ''' <value> The city. </value>
    Public Property City() As String

    ''' <summary> Gets or sets the state. </summary>
    ''' <value> The state. </value>
    Public Property State() As String

End Class
