Imports isr.Core.Capsule.ExceptionExtensions
Imports isr.Visuals.Reporting

''' <summary> Uses a chart to test reporting. </summary>
''' <remarks>
''' David, 2/15/2014, Reported. <para>
''' Launch this form by calling its Show or ShowDialog method from its default instance.
''' </para><para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class ReportTestPanel
    Inherits isr.Core.Forma.UserFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()

        ' instantiate objects that depend on resize.

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        Me._MessagesList.CommenceUpdates()

    End Sub


#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A message describing the status. </value>
    Private Property StatusMessage As String = String.Empty

    ''' <summary> Gets or sets the buffer for graphics data. </summary>
    ''' <value> A buffer for graphics data. </value>
    Private Property GraphicsBuffer As isr.Visuals.BufferedGraphics

    ''' <summary> Gets or sets the chart pane. </summary>
    ''' <value> The chart pane. </value>
    Private Property ChartPane As isr.Visuals.ChartPane

    ''' <summary> Creates the chart. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub CreateChart()
        Me.GraphicsBuffer = New isr.Visuals.BufferedGraphics(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me.ChartPane = New isr.Visuals.ChartPane
        Dim rec As New System.Drawing.Rectangle(Me._MessagesList.Location, Me._MessagesList.Size)
        Me.ChartPane.CreateSampleOne(rec)
    End Sub

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub InitializeUserInterface()

        ' set the combo box
        Me._ReportOptionsComboBox.Items.Clear()
        Me._ReportOptionsComboBox.Items.Add("select a report option:")
        Me._ReportOptionsComboBox.Items.Add("Report coded in the program")
        Me._ReportOptionsComboBox.Items.Add("Report coded in the program with auto paging")
        Me._ReportOptionsComboBox.Items.Add("Report from object data.")
        Me._ReportOptionsComboBox.Items.Add("Multi-Section report.")
        Me._ReportOptionsComboBox.Items.Add("Report from SQL data set.")
        Me._ReportOptionsComboBox.Items.Add("Report from SQL and object data.")
        Me._ReportOptionsComboBox.SelectedIndex = 0

        Me.CreateChart()

    End Sub

    ''' <summary> Terminates and disposes of class-level objects. </summary>
    ''' <remarks> Called from the form Closing method. </remarks>
    Private Sub TerminateObjects()
        Me._MessagesList.SuspendUpdatesReleaseIndicators()

        If Me._GraphicsBuffer IsNot Nothing Then
            Me.GraphicsBuffer.Dispose()
            Me._GraphicsBuffer = Nothing
        End If
        If Me._ChartPane IsNot Nothing Then
            Me._ChartPane.Dispose()
            Me._ChartPane = Nothing
        End If

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown.  This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                         <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      <see cref="System.EventArgs" /> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": REPORT TEST PANEL")

            ' set tool tips
            Me.InitializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '        loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Paints the chart. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="parent"> The parent. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.PaintEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub PaintChart(ByVal parent As Control, e As System.Windows.Forms.PaintEventArgs)

        If Not Me.IsDisposed AndAlso Not parent Is Nothing Then
            If Me.GraphicsBuffer.CanDoubleBuffer() Then

                Using brush As New SolidBrush(SystemColors.Window)
                    ' Fill in Background (for efficiency only the area that has been clipped)
                    ' Me.GraphicsBuffer.GraphicsDevice.FillRectangle(brush, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height)
                    Me.GraphicsBuffer.GraphicsDevice.FillRectangle(brush, e.ClipRectangle)
                End Using

                Using brush As New SolidBrush(Color.Gray)
                    ' clear the client area
                    Me.GraphicsBuffer.GraphicsDevice.FillRectangle(brush, parent.ClientRectangle)
                End Using

                ' Do our drawing using Me._graphicsBuffer.g instead e.Graphics
                Me.ChartPane.Draw(Me._GraphicsBuffer.GraphicsDevice)

                ' Render to the control.
                Me.GraphicsBuffer.Render(e.Graphics)

            Else
                ' if double buffer is not available, draw to e.Graphics

                ' clear
                Using brush As New SolidBrush(Color.Gray)
                    e.Graphics.FillRectangle(brush, parent.ClientRectangle)
                End Using

                ' draw
                Me._ChartPane.Draw(e.Graphics)

            End If

        End If

    End Sub

    ''' <summary> Handles the Paint event of the _ChartTabPage control. Updates the chart. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.PaintEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub ChartTabPage_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles _ChartTabPage.Paint
        If Not Me.IsDisposed AndAlso Not sender Is Nothing Then
            Me.PaintChart(CType(sender, Control), e)
        End If
    End Sub

    ''' <summary> Resizes the chart. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="parent"> The parent. </param>
    Private Sub ResizeChart(ByVal parent As Control)
        If Not Me.IsDisposed AndAlso Not parent Is Nothing Then
            If Me._GraphicsBuffer IsNot Nothing Then
                Me._GraphicsBuffer.CreateDoubleBuffer(parent.ClientRectangle.Width, parent.ClientRectangle.Height)
            End If
            If Me._ChartPane IsNot Nothing Then
                Me._ChartPane.SetSize(parent.ClientRectangle)
            End If
            parent.Invalidate()
        End If
    End Sub

    ''' <summary> Handles the Resize event of the _ChartTabPage control. Resizes the chart. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub ChartTabPage_Resize(sender As Object, e As System.EventArgs) Handles _ChartTabPage.Resize
        If Not Me.IsDisposed AndAlso Not sender Is Nothing Then
            Me.ResizeChart(CType(sender, Control))
        End If
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try

            Me._StatusLabel.Text = $"{DateTime.Now.Ticks} {CommandLineInfo.DevicesEnabled.GetValueOrDefault(True)}"
            Me.Close()

        Catch ex As Exception

            ' report failure 
            Me.StatusMessage = $"Failed closing {Environment.NewLine}{ex.ToFullBlownString}"
            System.Windows.Forms.MessageBox.Show(Me.StatusMessage, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        End Try

    End Sub

    ''' <summary> Call off button click. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CallOffButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    ''' <summary> Tests button click. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TestButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TestButton.Click

        ' check if a report option was selected
        If Me._ReportOptionsComboBox.SelectedIndex > 0 Then

            ' show report option selected
            Me._MessagesList.AddMessage($"Generating report: {Me._ReportOptionsComboBox.Text}")

            Select Case Me._ReportOptionsComboBox.SelectedIndex

                Case 0

                Case 1

                    Me.DoReportFromCode()

                Case 2

                    Me.DoReportAutoPaging()

                Case 3

                    DoReportFromObjectArrayList()

                Case 4

                    Me.DoMultiPartReport()

                Case 5

                    DoReportFromDataSet()

                Case 6

                    Me.DoReportFromultiPartReportDataSourcesb()

                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled report option")
            End Select

        End If

    End Sub

#End Region

#Region " REPORT FROM CODE "

        Private WithEvents CodedReport As isr.Visuals.Reporting.ReportDocument

    ''' <summary> Executes the report from code operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub DoReportFromCode()

        Me.CodedReport = New isr.Visuals.Reporting.ReportDocument

        Dim heading As Heading
        heading = Me.CodedReport.AddHeading(DateTimeOffset.Now.ToString(Globalization.CultureInfo.CurrentCulture), isr.Visuals.Reporting.HeadingType.SupHeader)
        heading.Appearance.Justification = isr.Visuals.Reporting.LineJustification.Left

        heading = Me.CodedReport.AddHeading("ISR Report Generator", isr.Visuals.Reporting.HeadingType.SupHeader)
        heading.Appearance.Justification = isr.Visuals.Reporting.LineJustification.Right

        heading = Me.CodedReport.AddHeading("Report from code (program generated)", isr.Visuals.Reporting.HeadingType.Header)
        heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

        heading = Me.CodedReport.AddHeading("Page {0} of {1}", HeadingType.SupFooter)
        heading.Appearance.Justification = LineJustification.Right
        heading.IsPage = True
        heading.IsPageCount = True
        heading.Visible = True

        heading = Me.CodedReport.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
        heading.Appearance.Justification = LineJustification.Left

        Me.CodedReport.Font = New Font("Ariel", 10)

        ' we have two pages
        Me.CodedReport.PageCount = 2

        ' print preview
        Me.CodedReport.Print(True)


    End Sub

    ''' <summary>
    ''' This event is raised for each page immediately after the header for the page has been
    ''' printed. The cursor is on the first line of the report body.
    ''' </summary>
    ''' <remarks> Use this method to print the body of the report. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Report page event information. </param>
    Private Sub CodedReport_PrintPageBodyStart(ByVal sender As Object, ByVal e As isr.Visuals.Reporting.ReportPageEventArgs) Handles CodedReport.PrintPageBodyStarted

        Dim reportDoc As isr.Visuals.Reporting.ReportDocument = CType(sender, isr.Visuals.Reporting.ReportDocument)

        If e.PageNumber = 1 Then

            e.WriteLine("Welcome to our report.")
            e.WriteLine()
            e.Write("We can print some text, and ")
            e.WriteLine("then put some more text on the same line.")
            e.WriteLine("This works much like writing to the console.")
            e.WriteLine()
            e.WriteLine()
            e.Write("We can left-justify", LineJustification.Left)
            e.Write("We can also center text", LineJustification.Centered)
            e.WriteLine("And we can right-justify", LineJustification.Right)
            e.WriteLine()
            e.WriteLine()
            e.WriteLine("Much simpler than doing this all by hand.")
            e.HasMorePages = reportDoc.PageCount > e.PageNumber

        Else

            e.WriteLine("This is page 2.")
            e.WriteLine("Just to show that we handle multiple pages.")

            ' add printing of graph
            Me._ChartPane.Print(e.Graphics, e.PageSettings.Bounds) '  New RectangleF(72, 288, 72, 288))

            ' Me._chartPane.Print(e.Graphics, e.PageSettings.Bounds, New Rectangle(72, 288, 72, 288))

            e.HasMorePages = reportDoc.PageCount > e.PageNumber

        End If


    End Sub

    ''' <summary> Coded report print page. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Print page event information. </param>
    Private Sub CodedReport_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles CodedReport.PrintPage

    End Sub

#End Region

#Region " REPORT WITH AUTO PAGING "

        Private WithEvents AutoPageReport As isr.Visuals.Reporting.ReportDocument

    ''' <summary> The consecutive line number. </summary>
    Private _ConsecutiveLineNumber As Int32

    ''' <summary> The lines to print. </summary>
    Private Const _LinesToPrint As Int32 = 200

    ''' <summary> Executes the report automatic paging operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub DoReportAutoPaging()
        Using autoPageReport As New isr.Visuals.Reporting.ReportDocument

            Dim heading As Heading
            heading = autoPageReport.AddHeading(DateTimeOffset.Now.ToString("F"), HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Left

            heading = autoPageReport.AddHeading("ISR Report Generator", HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Right

            heading = autoPageReport.AddHeading("Program generated report with auto paging", HeadingType.Header)
            heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

            heading = autoPageReport.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
            heading.Appearance.Justification = LineJustification.Left

            autoPageReport.Font = New Font("Ariel", 11)
            Me._ConsecutiveLineNumber = 0

            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = autoPageReport
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

    ''' <summary> Automatic page report print page body start. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Report page event information. </param>
    Private Sub AutoPageReport_PrintPageBodyStart(ByVal sender As Object,
                                                  ByVal e As isr.Visuals.Reporting.ReportPageEventArgs) Handles AutoPageReport.PrintPageBodyStarted

        While Me._ConsecutiveLineNumber < _LinesToPrint
            ' increment and print line number
            Me._ConsecutiveLineNumber += 1
            e.WriteLine($"Consecutive Line Number: {Me._ConsecutiveLineNumber:0000}")

            ' make sure we don't run off the end of the page
            If e.EndOfPage Then Exit While

        End While

        ' indicate whether we have more data to display
        If Me._ConsecutiveLineNumber < _LinesToPrint Then
            e.HasMorePages = True
        End If

    End Sub

#End Region

#Region " REPORT FROM DATA SET "

    ''' <summary> Executes the report from data set operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Shared Sub DoReportFromDataSet()

        ' load employees data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Dim query As String = "SELECT emp_id,FNAME,LNAME FROM employee"
        Using da As New System.Data.SqlClient.SqlDataAdapter(query, dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)
                ' initialize report
                Using report As New isr.Visuals.Reporting.ReportDocument
                    Dim heading As Heading
                    heading = report.AddHeading(DateTimeOffset.Now.ToString("G"), HeadingType.SupHeader)
                    heading.Appearance.Justification = LineJustification.Left

                    heading = report.AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    heading.Appearance.Justification = LineJustification.Right

                    heading = report.AddHeading("Pubs Employees", HeadingType.Header)
                    heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

                    heading = report.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    heading.Appearance.Justification = LineJustification.Left

                    report.Font = New Font("Ariel", 10)

                    ' set the data source, using auto discover to find the columns
                    report.AutoDiscover = True
                    report.DataSource = ds

                    ' override the column names to be more human readable
                    report.Columns(0).Name = "Employee id"
                    report.Columns(1).Name = "First name"
                    report.Columns(2).Name = "Last name"

                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = report
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using
            End Using

        End Using

    End Sub

#End Region

#Region " REPORT FROM OBJECTS "

    ''' <summary> Executes the report from object array list operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Shared Sub DoReportFromObjectArrayList()

        ' load some data
        Dim data As New ArrayList From {
            New Person("John", "Bloomington", "MN"),
            New Person("Mary", "Bloomington", "IL"),
            New Person("Aaron", "Minneapolis", "MN"),
            New Person("Ben", "San Francisco", "CA")
        }

        ' initialize report
        Using report As New isr.Visuals.Reporting.ReportDocument

            Dim heading As Heading
            heading = report.AddHeading(DateTimeOffset.Now.ToString("G"), HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Left

            heading = report.AddHeading("ISR Report Generator", HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Right

            heading = report.AddHeading("Report from Person Objects", HeadingType.Header)
            heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

            heading = report.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
            heading.Appearance.Justification = LineJustification.Left

            report.Font = New Font("Ariel", 10)

            ' set the data source, using auto discover to find the columns
            report.AutoDiscover = True
            report.DataSource = data

            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = report
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

#End Region

#Region " REPORT WITH MULTIPLE SECTIONS "

        Private WithEvents MultiPartReport As isr.Visuals.Reporting.ReportDocument

    ''' <summary> List of object arrays. </summary>
    Private ReadOnly _ObjectArrayList As New ArrayList

    ''' <summary> Executes the multi part report operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub DoMultiPartReport()

        ' load some data into a collection of objects
        Me._ObjectArrayList.Add(New Person("John", "Bloomington", "MN"))
        Me._ObjectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
        Me._ObjectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
        Me._ObjectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

        ' initialize report to report against DataSet
        Using multiPartReport As New isr.Visuals.Reporting.ReportDocument
            Me.MultiPartReport = multiPartReport
            Dim heading As Heading
            heading = multiPartReport.AddHeading(DateTimeOffset.Now.ToString("G"), HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Left

            heading = multiPartReport.AddHeading("ISR Report Generator", HeadingType.SupHeader)
            heading.Appearance.Justification = LineJustification.Right

            heading = multiPartReport.AddHeading("Multi-Section Report", HeadingType.Header)
            heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

            heading = multiPartReport.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
            heading.Appearance.Justification = LineJustification.Left

            multiPartReport.Font = New Font("Ariel", 10)

            ' set the data source, using auto discover to find the columns
            multiPartReport.AutoDiscover = False

            ' start with the manual portion of the report section 1.
            multiPartReport.Section = 1


            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = multiPartReport
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

    ''' <summary> Executes the report fromulti part report data sourcesb operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub DoReportFromultiPartReportDataSourcesb()

        ' load authors data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Using da As New System.Data.SqlClient.SqlDataAdapter("SELECT emp_id,FNAME,LNAME FROM employee", dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)
                ' load some data into a collection of objects
                Me._ObjectArrayList.Add(New Person("John", "Bloomington", "MN"))
                Me._ObjectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
                Me._ObjectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
                Me._ObjectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

                ' initialize report to report against DataSet
                Using multiPartReport As New isr.Visuals.Reporting.ReportDocument
                    Me.MultiPartReport = multiPartReport

                    Dim heading As Heading
                    heading = multiPartReport.AddHeading(DateTimeOffset.Now.ToString("G"), HeadingType.SupHeader)
                    heading.Appearance.Justification = LineJustification.Left

                    Me._SupHeaderRight = multiPartReport.AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    Me._SupHeaderRight.Appearance.Justification = LineJustification.Right

                    heading = multiPartReport.AddHeading("Multi-Part Report", HeadingType.Header)
                    heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

                    heading = multiPartReport.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    heading.Appearance.Justification = LineJustification.Left

                    multiPartReport.Font = New Font("Ariel", 10)

                    ' set the data source, using auto discover to find the columns
                    multiPartReport.AutoDiscover = True
                    multiPartReport.DataSource = ds

                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = multiPartReport
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using

            End Using
        End Using


    End Sub

    ''' <summary>
    ''' This event is raised for each page immediately after the header for the page has been
    ''' printed. The cursor is on the first line of the report body.
    ''' </summary>
    ''' <remarks> Use this method to print the body of the report. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Report page event information. </param>
    Private Sub MultiPartReport_PrintPageBodyStart(ByVal sender As Object,
                                                   ByVal e As isr.Visuals.Reporting.ReportPageEventArgs) Handles MultiPartReport.PrintPageBodyStarted

        If Me.MultiPartReport.Section = 1 Then

            ' if manual portion, do the manual part.
            If e.PageNumber = 1 Then

                e.WriteLine("Welcome to our report.")
                e.WriteLine()
                e.Write("We can print some text, and ")
                e.WriteLine("then put some more text on the same line.")
                e.WriteLine("This works much like writing to the console.")
                e.WriteLine()
                e.WriteLine()
                e.Write("We can left-justify", LineJustification.Left)
                e.Write("We can also center text", LineJustification.Centered)
                e.WriteLine("And we can right-justify", LineJustification.Right)
                e.WriteLine()
                e.WriteLine()
                e.WriteLine("Much simpler than doing this all by hand.")
                e.HasMorePages = True

            Else

                e.WriteLine("This is page 2.")
                e.WriteLine("Just to show that we handle multiple pages.")
                e.HasMorePages = False

            End If
        End If

    End Sub

    ''' <summary> Multi part report end. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Report page event information. </param>
    Private Sub MultiPartReport_ReportEnd(ByVal sender As Object, ByVal e As isr.Visuals.Reporting.ReportPageEventArgs) Handles MultiPartReport.ReportEnded

        ' if we just reported the DataSet we don't want the 
        ' report to end, so override to also report
        ' the objects
        If TypeOf Me.MultiPartReport.DataSource Is DataSet Then

            ' indicate there ARE more pages
            e.HasMorePages = True

            ' DateTimeOffset.Now set the data source to our collection
            Me.MultiPartReport.DataSource = Me._ObjectArrayList

            ' set the data source, using auto discover to find the columns
            Me.MultiPartReport.AutoDiscover = True

            ' change the subtitle to indicate the new type of data
            Me._SupHeaderRight.Appearance.Text = "Person Objects"

            ' turn on the second section of the report
            Me.MultiPartReport.Section = 2

        End If

    End Sub

#End Region

#Region " REPORT WITH MULTIPLE DATA SETS "

        Private WithEvents MultiDataSetsReport As isr.Visuals.Reporting.ReportDocument

    ''' <summary> The sup header right. </summary>
    Private _SupHeaderRight As Heading

    ''' <summary> Executes the multi data report operation. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub DoMultiDataReport()

        ' load authors data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Using da As New System.Data.SqlClient.SqlDataAdapter("SELECT emp_id,FNAME,LNAME FROM employee", dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)

                ' load some data into a collection of objects
                Me._ObjectArrayList.Add(New Person("John", "Bloomington", "MN"))
                Me._ObjectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
                Me._ObjectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
                Me._ObjectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

                ' initialize report to report against DataSet
                Using multiDataSetsReport As New isr.Visuals.Reporting.ReportDocument
                    Me.MultiDataSetsReport = multiDataSetsReport

                    Dim heading As Heading
                    heading = multiDataSetsReport.AddHeading(DateTimeOffset.Now.ToString("G"), HeadingType.SupHeader)
                    heading.Appearance.Justification = LineJustification.Left

                    Me._SupHeaderRight = multiDataSetsReport.AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    Me._SupHeaderRight.Appearance.Justification = LineJustification.Right

                    heading = multiDataSetsReport.AddHeading("Report with multiple data sets", HeadingType.Header)
                    heading.Appearance.Font = New Font(heading.Appearance.Font, FontStyle.Bold)

                    heading = multiDataSetsReport.AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    heading.Appearance.Justification = LineJustification.Left

                    multiDataSetsReport.Font = New Font("Ariel", 10)

                    ' set the data source, using auto discover to find the columns
                    multiDataSetsReport.AutoDiscover = True
                    multiDataSetsReport.DataSource = ds

                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = Me.MultiPartReport
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using

            End Using
        End Using

    End Sub

    ''' <summary> Multi data sets report end. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object" /> instance of this
    '''                       <see cref="System.Windows.Forms.Form" /> </param>
    ''' <param name="e">      Report page event information. </param>
    Private Sub MultiDataSetsReport_ReportEnd(ByVal sender As Object,
                                              ByVal e As isr.Visuals.Reporting.ReportPageEventArgs) Handles MultiDataSetsReport.ReportEnded

        ' if we just reported the DataSet we don't want the 
        ' report to end, so override to also report
        ' the objects
        If TypeOf Me.MultiDataSetsReport.DataSource Is DataSet Then

            ' indicate there ARE more pages
            e.HasMorePages = True

            ' DateTimeOffset.Now set the data source to our collection
            Me.MultiPartReport.DataSource = Me._ObjectArrayList

            ' set the data source, using auto discover to find the columns
            Me.MultiPartReport.AutoDiscover = True

            ' change the subtitle to indicate the new type of data
            Me._SupHeaderRight.Appearance.Text = "Person Objects"

        End If

    End Sub

#End Region

End Class
