Imports System.Collections.Generic

Imports isr.Core.Capsule.ExceptionExtensions

''' <summary> Selects a test panel. </summary>
''' <remarks>
''' Launch this form by calling its Show or ShowDialog method from its default instance.  <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 2/15/2014, Documented. </para>
''' </remarks>
Public Class Switchboard
    Inherits isr.Core.Forma.UserFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        Me._MessagesList.CommenceUpdates()

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A message describing the status. </value>
    Private Property StatusMessage As String = String.Empty

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Enumerates the available test panels. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Enum TestPanel

        ''' <summary> An enum constant representing the none option. </summary>
        <System.ComponentModel.Description("Not selected")> None

        ''' <summary> An enum constant representing the reports option. </summary>
        <System.ComponentModel.Description("Reports")> Reports
    End Enum

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub InitializeUserInterface()
        Me._PanelComboBox.Items.Clear()
        ' Me.panelComboBox.DataSource = [Enum].GetNames(GetType(TestPanel))
        Me._PanelComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(TestPanel)).ToList
        Me._PanelComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._PanelComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs before the form is closed. </summary>
    ''' <remarks>
    ''' Use this method to optionally cancel the closing of the form. Because the form is not yet
    ''' closed at this point, this is also the best place to serialize a form's visible properties,
    ''' such as size and location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the form closed.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                         <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
    Private Sub Form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
            Me._MessagesList.SuspendUpdatesReleaseIndicators()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown.  This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                         <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SWITCHBOARD")

            ' set tool tips
            Me.InitializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '      loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Tester activated. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Tester_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Me._MessagesList.AddMessage("Activated")
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try

            Me._StatusLabel.Text = $"{DateTime.Now.Ticks} {CommandLineInfo.DevicesEnabled.GetValueOrDefault(True)}"
            Me.Close()

        Catch ex As Exception

            ' report failure 
            Me.StatusMessage = $"Failed closing {Environment.NewLine}{ex.ToFullBlownString}"
            System.Windows.Forms.MessageBox.Show(Me.StatusMessage, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        End Try

    End Sub

    ''' <summary> Call off button click. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CallOffButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    ''' <summary> Opens the selected form. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenButton.Click
        Dim selectedKeyValuePair As System.Collections.Generic.KeyValuePair(Of [Enum], String)
        selectedKeyValuePair = CType(Me._PanelComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String))
        Select Case CType(selectedKeyValuePair.Key, TestPanel)
            Case TestPanel.Reports
                Using r As New ReportTestPanel
                    r.ShowDialog()
                End Using
        End Select
    End Sub

#End Region

End Class
