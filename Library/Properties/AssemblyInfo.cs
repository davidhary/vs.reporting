﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Visuals.Reporting.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Visuals.Reporting.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Visuals.Reporting.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
