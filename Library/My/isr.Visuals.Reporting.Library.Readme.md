## ISR Visuals Reporting Library<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*3.1.6667 2018-04-03*  
2018 release.

*2.1.4767 2013-01-18*  
Adds printer setup to the print preview dialog.

*2.1.4720 2012-12-03*  
Updated to VS 2010.

*2.1.4232 2011-08-03*  
Standardizes code elements and documentation.

*2.1.4213 2011-07-15*  
Simplifies the assembly information.

*2.1.2961 2008-02-09*  
Updated to .NET 3.5.

*2.0.2789 2007-08-21*  
Updated to Visual Studio 8.

*1.0.2711 2007-06-04*  
Limits rounding digits between 0 and 15.

*1.0.2228 2006-02-06*  
Replaces Is Filled, Is Bold, etc. with Filled, Bold,
etc.

*1.0.2219 2006-01-28*  
Split from the drawing library.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Reporting Library](https://bitbucket.org/davidhary/vs.reporting):
