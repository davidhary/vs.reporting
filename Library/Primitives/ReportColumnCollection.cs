using System;

namespace isr.Visuals.Reporting
{

    /// <summary>
    /// Defines a strongly-typed collection that contains
    /// <see cref="T:isr.Visuals.Printing.ReportColumn" /> objects.
    /// </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed class ReportColumnCollection : System.Collections.ObjectModel.Collection<ReportColumn>, ICloneable
    {

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Default constructor for the collection class. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public ReportColumnCollection() : base()
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The ReportColumnCollection object from which to copy. </param>
        public ReportColumnCollection( ReportColumnCollection model ) : base()
        {
            if ( model is null )
                throw new ArgumentNullException( nameof( model ) );
            foreach ( var item in model )
                this.Add( new ReportColumn( item ) );
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the ArrowCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the ArrowCollection. </returns>
        public ReportColumnCollection Copy()
        {
            return new ReportColumnCollection( this );
        }

        /// <summary>
        /// Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
        /// field name. The Name and Field of the column are set to the provided field name. The Left and
        /// Width values are 0 and must be set separately.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="field"> The name of the data field. </param>
        public void Add( string field )
        {
            if ( string.IsNullOrWhiteSpace( field ) )
                throw new ArgumentNullException( nameof( field ) );
            this.Add( new ReportColumn() {
                Name = field,
                Field = field
            } );
        }

        /// <summary>
        /// Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
        /// field name. The Name and Field of the column are set to the provided field name. The Left
        /// value is set to the provided value. The Width value is 0 and must be set separately.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="field"> The name of the data field. </param>
        /// <param name="left">  The X position of the column. </param>
        public void Add( string field, int left )
        {
            if ( string.IsNullOrWhiteSpace( field ) )
                throw new ArgumentNullException( nameof( field ) );
            this.Add( new ReportColumn() {
                Name = field,
                Field = field,
                Left = left
            } );
        }

        /// <summary>
        /// Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
        /// field name. The Name and Field of the column are set to the provided values. The Left value
        /// is set to the provided value. The Width value is 0 and must be set separately.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">  The human-readable column name. </param>
        /// <param name="field"> The name of the data field. </param>
        /// <param name="left">  The X position of the column. </param>
        public void Add( string name, string field, int left )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            if ( string.IsNullOrWhiteSpace( field ) )
                throw new ArgumentNullException( nameof( field ) );
            this.Add( new ReportColumn() {
                Name = name,
                Field = field,
                Left = left
            } );
        }

        /// <summary>
        /// Called by the data binding mechanism to automatically run through all the columns defined by
        /// this collection and to set their widths to evenly consume all the horizontal space on a line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="width"> The total width of a printed line. </param>
        internal void SetEvenSpacing( int width )
        {
            int space = Convert.ToInt32( width / ( double ) this.Count );
            int index;
            var loopTo = this.Count - 1;
            for ( index = 0; index <= loopTo; index++ )
            {
                var col = base[index];
                col.Left = space * index;
                col.Width = space;
            }
        }

        #endregion

    }
}
