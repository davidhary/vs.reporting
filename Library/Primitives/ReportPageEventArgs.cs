using System;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals.Reporting
{

    /// <summary>
    /// This is a list of the possible text justification values used by the
    /// <see cref="M:isr.Visuals.Printing.ReportPageEventArgs.Write(System.String,isr.Visuals.Reporting.LineJustification)" />
    /// and
    /// <see cref="M:isr.Visuals.Printing.ReportPageEventArgs.WriteLine(System.String,isr.Visuals.Reporting.LineJustification)" />
    /// methods.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public enum LineJustification
    {

        /// <summary>
        /// The none
        /// </summary>
        None,

        /// <summary>
        /// The left
        /// </summary>
        Left,

        /// <summary>
        /// The centered
        /// </summary>
        Centered,

        /// <summary>
        /// The right
        /// </summary>
        Right
    }

    /// <summary>
    /// The ReportPageEventArgs the type of the parameter provided by the events raised from the
    /// <see cref="T:isr.Visuals.Printing.ReportDocument" />
    /// object. This class includes methods to simplify the process of rendering text output into
    /// each page of the report.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ReportPageEventArgs : System.Drawing.Printing.PrintPageEventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> This is the main constructor method for this class. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="e">           The <see cref="System.Drawing.Printing.PrintPageEventArgs" />
        /// instance containing the event data. </param>
        /// <param name="pageNumber">  The page number. </param>
        /// <param name="font">        The font. </param>
        /// <param name="brush">       The brush. </param>
        /// <param name="footerLines"> The footer lines. </param>
        internal ReportPageEventArgs( System.Drawing.Printing.PrintPageEventArgs e, int pageNumber, Font font, Brush brush, int footerLines ) : base( e.Graphics, e.MarginBounds, e.PageBounds, e.PageSettings )
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            if ( brush is null )
            {
                throw new ArgumentNullException( nameof( brush ) );
            }

            this.PageNumber = pageNumber;
            this._Font = font;
            this.Brush = brush;
            this.PositionToStart();
            this.FooterLines = footerLines;
            this.LineHeight = Convert.ToInt32( this._Font.GetHeight( this.Graphics ) );
            this._PageBottom = this.MarginBounds.Bottom - this.FooterLines * this.LineHeight - this.LineHeight;
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Returns True if the cursor's current location is beyond the bottom of the page body. This
        /// doesn't mean we're into the bottom margin, but may indicate that the cursor in the page's
        /// footer region.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A Boolean indicating whether the cursor is past the end of the page. </returns>
        public bool EndOfPage()
        {
            return this.CurrentY >= this._PageBottom;
        }

        /// <summary>
        /// Returns True if the cursor's current location after printing the text will be beyond the
        /// bottom of the page body.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text"> Text to consider for printing. </param>
        /// <returns> A Boolean indicating whether the cursor is past the end of the page. </returns>
        public bool EndOfPage( string text )
        {
            return string.IsNullOrWhiteSpace( text ) ? this.CurrentY >= this._PageBottom : this.CurrentY + this.Graphics.MeasureString( text, this._Font ).Height >= this._PageBottom;
        }

        /// <summary>
        /// Returns True if the cursor's current location after printing the text will be beyond the
        /// bottom of the page body.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text">       Text to consider for printing. </param>
        /// <param name="layoutArea"> The <see cref="System.Drawing.SizeF">layout area</see> where the teat
        /// is to be rendered. </param>
        /// <returns> A Boolean indicating whether the cursor is past the end of the page. </returns>
        public bool EndOfPage( string text, SizeF layoutArea )
        {
            return string.IsNullOrWhiteSpace( text ) ? this.CurrentY >= this._PageBottom : this.CurrentY + this.Graphics.MeasureString( text, this._Font, layoutArea ).Height >= this._PageBottom;
        }

        /// <summary> Returns the position for printing the text. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">          The text to render. </param>
        /// <param name="font">          The font. </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        /// <returns> System.Int32. </returns>
        private int GetPosition( string text, Font font, LineJustification justification )
        {

            // set current position to write based on justification
            switch ( justification )
            {
                case LineJustification.None:
                    {

                        // use current position
                        return this.CurrentX;
                    }

                case LineJustification.Left:
                    {
                        return this.MarginBounds.Left;
                    }

                case LineJustification.Centered:
                    {
                        return string.IsNullOrWhiteSpace( text )
                            ? this.MarginBounds.Left + Convert.ToInt32( this.MarginBounds.Width / 2d )
                            : font is null
                                ? throw new ArgumentNullException( nameof( font ) )
                                : this.MarginBounds.Left + Convert.ToInt32( this.MarginBounds.Width / 2d - this.Graphics.MeasureString( text, font ).Width / 2f );

                        break;
                    }

                case LineJustification.Right:
                    {
                        return string.IsNullOrWhiteSpace( text )
                            ? Convert.ToInt32( this.MarginBounds.Right )
                            : font is null
                                ? throw new ArgumentNullException( nameof( font ) )
                                : Convert.ToInt32( this.MarginBounds.Right - this.Graphics.MeasureString( text, font ).Width );

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled justification type" );
                        break;
                    }
            }

            return default;
        }

        /// <summary> Returns the position for printing the text. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text">          The text to render. </param>
        /// <param name="font">          The font. </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        /// <param name="layoutArea">    The layout area <see cref="RectangleF" /> </param>
        /// <returns> System.Int32. </returns>
        private int GetPosition( string text, Font font, LineJustification justification, RectangleF layoutArea )
        {

            // set current position to write based on justification
            switch ( justification )
            {
                case LineJustification.None:
                    {

                        // use current position
                        return this.CurrentX;
                    }

                case LineJustification.Left:
                    {
                        return Convert.ToInt32( layoutArea.X );
                    }

                case LineJustification.Centered:
                    {
                        return string.IsNullOrWhiteSpace( text ) ? Convert.ToInt32( layoutArea.Left + layoutArea.Width / 2f ) : Convert.ToInt32( layoutArea.Left + layoutArea.Width / 2f ) - Convert.ToInt32( this.Graphics.MeasureString( text, font ).Width / 2f );
                    }

                case LineJustification.Right:
                    {
                        return string.IsNullOrWhiteSpace( text ) ? Convert.ToInt32( layoutArea.Right ) : Convert.ToInt32( layoutArea.Right - this.Graphics.MeasureString( text, font ).Width );
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled justification type" );
                        break;
                    }
            }

            return default;
        }

        /// <summary>
        /// Draws a horizontal line across the width of the page on the current line. After the line is
        /// drawn the cursor is moved down one line and to the left side of the page.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void HorizontalLine()
        {
            int y = this.CurrentY + Convert.ToInt32( this.LineHeight / 2d );
            this.Graphics.DrawLine( Pens.Black, this.MarginBounds.Left, y, this.MarginBounds.Right, y );
            this.WriteLine();
        }

        /// <summary> Moves the cursor to the top left corner of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void PositionToFooter()
        {
            this.CurrentX = this.MarginBounds.Left;
            this.CurrentY = this.MarginBounds.Bottom;
        }

        /// <summary> Moves the cursor to the top left corner of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void PositionToHeader()
        {
            this.CurrentX = this.MarginBounds.Left;
            this.CurrentY = this.MarginBounds.Top / 2;
        }

        /// <summary> Moves the cursor to the top left corner of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void PositionToStart()
        {
            this.CurrentX = this.MarginBounds.Left;
            this.CurrentY = this.MarginBounds.Top;
        }

        /// <summary>
        /// Writes some text to the report starting at the current cursor location. The cursor is moved
        /// to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text"> The text to render. </param>
        public void Write( string text )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                this.Graphics.DrawString( text, this._Font, this.Brush, this.CurrentX, this.CurrentY );
                this.CurrentX += Convert.ToInt32( this.Graphics.MeasureString( text, this._Font ).Width );
            }
        }

        /// <summary>
        /// Writes some text to the report starting at the current cursor location. The cursor is moved
        /// to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">  The text to render. </param>
        /// <param name="font">  The <see cref="Font" /> </param>
        /// <param name="brush"> The <see cref="System.Drawing.Brush">Brush</see> </param>
        public void Write( string text, Font font, Brush brush )
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            if ( brush is null )
            {
                throw new ArgumentNullException( nameof( brush ) );
            }

            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                this.Graphics.DrawString( text, font, brush, this.CurrentX, this.CurrentY );
                this.CurrentX += Convert.ToInt32( this.Graphics.MeasureString( text, this._Font ).Width );
            }
        }

        /// <summary>
        /// Writes some text to the report starting at the current cursor location. The cursor is moved
        /// to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">       The text to render. </param>
        /// <param name="font">       The <see cref="Font" /> </param>
        /// <param name="brush">      The <see cref="System.Drawing.Brush">Brush</see> </param>
        /// <param name="layoutArea"> The <see cref="SizeF">size</see> of the area allocated for writing. </param>
        public void Write( string text, Font font, Brush brush, SizeF layoutArea )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                if ( font is null )
                {
                    throw new ArgumentNullException( nameof( font ) );
                }

                if ( brush is null )
                {
                    throw new ArgumentNullException( nameof( brush ) );
                }

                this.Graphics.DrawString( text, font, brush, new RectangleF( this.CurrentX, this.CurrentY, layoutArea.Width, layoutArea.Height ) );
            }
        }

        /// <summary>
        /// Writes text to the report on the current line, but justified based on the justification
        /// parameter value. The cursor is moved to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">          The text to render. </param>
        /// <param name="font">          The <see cref="Font" /> </param>
        /// <param name="brush">         The <see cref="System.Drawing.Brush">Brush</see> </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        public void Write( string text, Font font, Brush brush, LineJustification justification )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {

                // validate arguments
                if ( font is null )
                {
                    throw new ArgumentNullException( nameof( font ) );
                }

                if ( brush is null )
                {
                    throw new ArgumentNullException( nameof( brush ) );
                }

                this.CurrentX = this.GetPosition( text, font, justification );

                // write the text.
                this.Write( text, font, brush );
            }
        }

        /// <summary>
        /// Writes text to the report on the current line, but justified based on the justification
        /// parameter value. The cursor is moved to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">          The text to render. </param>
        /// <param name="font">          The <see cref="Font" /> </param>
        /// <param name="brush">         The <see cref="System.Drawing.Brush">Brush</see> </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        /// <param name="layoutArea">    The layout area <see cref="RectangleF" /> </param>
        public void Write( string text, Font font, Brush brush, LineJustification justification, RectangleF layoutArea )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {

                // validate arguments
                if ( font is null )
                {
                    throw new ArgumentNullException( nameof( font ) );
                }

                if ( brush is null )
                {
                    throw new ArgumentNullException( nameof( brush ) );
                }

                this.CurrentX = this.GetPosition( text, font, justification, layoutArea );

                // write the text.
                this.Write( text, font, brush, layoutArea.Size );
            }
        }

        /// <summary>
        /// Writes text to the report on the current line, but justified based on the justification
        /// parameter value. The cursor is moved to the right, but not down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text">          The text to render. </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        public void Write( string text, LineJustification justification )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                this.CurrentX = this.GetPosition( text, this._Font, justification );

                // write the text.
                this.Write( text );
            }
        }

        /// <summary>
        /// Writes text into a specific column within the report on the current line. It uses a
        /// <see cref="T:isr.Visuals.Printing.ReportColumn" />
        /// object to define the X position and width of the column. The cursor is not moved by calling
        /// this method.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">   The text to render into the column. </param>
        /// <param name="column"> The <see cref="T:isr.Visuals.Printing.ReportColumn" /> object defining
        /// this column. </param>
        public void WriteColumn( string text, ReportColumn column )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                if ( column is null )
                {
                    throw new ArgumentNullException( nameof( column ) );
                }

                int x = this.MarginBounds.Left + column.Left;
                this.Graphics.FillRectangle( Brushes.White, new Rectangle( x - 5, this.CurrentY, column.Width + 5, this.LineHeight ) );
                this.Graphics.DrawString( text, this._Font, this.Brush, x, this.CurrentY );
            }
        }

        /// <summary> Moves the cursor down one line and to the left side of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="verticalShift"> The amount of vertical shift in 0.1 inches. </param>
        public void WriteLine( int verticalShift )
        {
            this.CurrentX = this.MarginBounds.Left;
            this.CurrentY += verticalShift;
        }

        /// <summary> Moves the cursor down one line and to the left side of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void WriteLine()
        {
            this.WriteLine( this.LineHeight );
        }

        /// <summary> Moves the cursor down one line and to the left side of the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="font"> The <see cref="Font" /> </param>
        public void WriteLine( Font font )
        {

            // validate arguments
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            this.WriteLine( Convert.ToInt32( font.GetHeight( this.Graphics ) ) );
        }

        /// <summary>
        /// Writes text to the report starting at the current cursor location and then moves the cursor
        /// down one line and to the left side of the page.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text"> The text. </param>
        public void WriteLine( string text )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {
                this.Graphics.DrawString( text, this._Font, this.Brush, this.CurrentX, this.CurrentY );
                this.WriteLine();
            }
        }

        /// <summary>
        /// Writes text to the report on the current line, but justified based on the justification
        /// parameter value. The cursor is moved down to the next line.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text">          The text. </param>
        /// <param name="justification"> The <see cref="Reporting.LineJustification">justification</see>
        /// for the text. </param>
        public void WriteLine( string text, LineJustification justification )
        {
            if ( !string.IsNullOrWhiteSpace( text ) )
            {

                // get new position 
                this.CurrentX = this.GetPosition( text, this._Font, justification );

                // write the text
                this.Write( text );

                // move to the next line
                this.WriteLine();
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> gets or sets the brush. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The brush. </value>
        public Brush Brush { get; set; }

        /// <summary>
        /// Gets or sets or returns the current X position (left to right) of the cursor on the page.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The horizontal position of the cursor. </value>
        public int CurrentX { get; set; }

        /// <summary>
        /// Gets or sets or returns the current Y position (top to bottom) of the cursor on the page.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The vertical position of the cursor. </value>
        public int CurrentY { get; set; }

        /// <summary> The font. </summary>
        private Font _Font;

        /// <summary> Gets or sets the font. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The font. </value>
        public Font Font
        {
            get => this._Font;

            set {
                if ( value is null )
                    throw new ArgumentNullException( nameof( value ) );
                this._Font = value;
                this.LineHeight = Convert.ToInt32( this._Font.GetHeight( this.Graphics ) );
            }
        }

        /// <summary> Gets or gets the number of footer lines. </summary>
        /// <value> The footer lines. </value>
        public int FooterLines { get; set; }

        /// <summary> Gets or gets the line height.  It is reset when setting the font. </summary>
        /// <value> The height of the line. </value>
        public int LineHeight { get; set; }

        /// <summary> Gets the number of pages.  This must be set by the report calling method. </summary>
        /// <value> The page count. </value>
        public int PageCount { get; set; }

        /// <summary>
        /// The Me._page bottom
        /// </summary>
        private readonly int _PageBottom;

        /// <summary>
        /// Returns the Y value corresponding to the bottom of the page body. This is the position
        /// immediately above the start of the page footer.
        /// </summary>
        /// <value> The Y value of the bottom of the page. </value>
        public int PageBottom => this._PageBottom + this.LineHeight;

        /// <summary>
        /// Returns the page number of the current page. This value is automatically incremented as each
        /// new page is rendered.
        /// </summary>
        /// <value> The current page number. </value>
        public int PageNumber { get; private set; }

        #endregion

    }
}
