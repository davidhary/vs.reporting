using System;

namespace isr.Visuals.Reporting
{

    /// <summary>
    /// Defines a column into which text can be rendered on a line of a table when the
    /// <see cref="T:.Printing.Report.ReportDocument" />
    /// is bound to a data source.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ReportColumn : IDisposable, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-27. </remarks>

        // instantiate the base class
        public ReportColumn() : base()
        {
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks>
        /// Use this constructor to instantiate this class and set the instance name, which is useful in
        /// tracing.
        /// </remarks>
        /// <param name="instanceName"> Specifies the name of the instance. </param>

        // instantiate the base class
        public ReportColumn( string instanceName ) : this()
        {
            this._InstanceName = instanceName;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The ReportColumn object from which to copy. </param>
        public ReportColumn( ReportColumn model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Field = model.Field;
            this._InstanceName = model._InstanceName;
            this.Left = model.Left;
            this.Name = model.Name;
            this.Width = model.Width;
            this.StatusMessage = model.StatusMessage;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status sentinel. </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( !(this.IsDisposed == true) )
                    {

                        // Free managed resources when explicitly called
                        this.StatusMessage = string.Empty;
                        this._InstanceName = string.Empty;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public ReportColumn Copy()
        {
            return new ReportColumn( this );
        }

        /// <summary> Overrides ToString returning the instance name if not empty. </summary>
        /// <remarks>
        /// Use this method to return the instance name. If instance name is not set, returns the base
        /// class ToString value.
        /// </remarks>
        /// <returns> A <see cref="System.String" /> that represents this instance. </returns>
        public override string ToString()
        {
            return string.IsNullOrWhiteSpace( this._InstanceName ) ? base.ToString() : this._InstanceName;
        }

        /// <summary>
        /// The Me._instance name
        /// </summary>
        private string _InstanceName = string.Empty;

        /// <summary> Gets or sets the name given to an instance of this class. </summary>
        /// <value> <c>InstanceName</c> is a String property. </value>
        public string InstanceName
        {
            get => this.ToString();

            set => this._InstanceName = value;
        }

        /// <summary> Gets the status message. </summary>
        /// <value> A <see cref="System.String">String</see>. </value>
        public string StatusMessage { get; set; }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Defines the human-readable name of the column. This value can be useful for generating
        /// descriptive headers.
        /// </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary>
        /// Contains the name of the field within the data source that contains the data. This value is
        /// used to retrieve the data value from the data source. It corresponds to the column name in a
        /// DataTable, or a property name of an object.
        /// </summary>
        /// <value> The field. </value>
        public string Field { get; set; }

        /// <summary>
        /// Defines the horizontal start location (X coordinate) of the column. When text is written to
        /// the column by the
        /// <see cref="M:isr.Visuals.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Visuals.Printing.ReportColumn)" />
        /// method
        /// it is rendered starting at this horizontal location.
        /// </summary>
        /// <value> The left. </value>
        public int Left { get; set; }

        /// <summary>
        /// Defines the width of the column. Before text is written to the column by the
        /// <see cref="M:isr.Visuals.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Visuals.Printing.ReportColumn)" />
        /// method
        /// the column is filled with a white rectangle defined by the width of the column. This helps
        /// prevent text from overwriting other text within our columns.
        /// </summary>
        /// <value> The width. </value>
        internal int Width { get; set; }

        #endregion

        #region " PRIVATE  and  PROTECTED "

        #endregion

    }
}
