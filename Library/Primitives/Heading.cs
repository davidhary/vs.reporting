using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals.Reporting
{

    /// <summary> Handles specification and drawing of report headings. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Heading : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="ReportDocument">report</see> title sets all title
        /// properties to default values as defined in the <see cref="HeaderDefaults" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="type"> The heading <see cref="HeadingType">type</see> </param>
        public Heading( HeadingType type ) : base()
        {
            this.Appearance = new TextAppearance( string.Empty, ( Font ) TitleDefaults.Font.Clone(), ( Brush ) TitleDefaults.Brush.Clone(), TitleDefaults.Justification );
            this.HeadingType = HeadingType.Title;
            this.Visible = TitleDefaults.Visible;
            this.StatusMessage = string.Empty;
            this.HeadingType = type;
            switch ( this.HeadingType )
            {
                case HeadingType.Footer:
                case HeadingType.SubFooter:
                case HeadingType.SupFooter:
                    {
                        this.Visible = FooterDefaults.Visible;
                        this.Appearance.Brush = ( Brush ) FooterDefaults.Brush.Clone();
                        this.Appearance.Font = ( Font ) FooterDefaults.Font.Clone();
                        this.Appearance.Justification = FooterDefaults.Justification;
                        break;
                    }

                case HeadingType.Header:
                case HeadingType.SubHeader:
                case HeadingType.SupHeader:
                    {
                        this.Visible = HeaderDefaults.Visible;
                        this.Appearance.Brush = ( Brush ) HeaderDefaults.Brush.Clone();
                        this.Appearance.Font = ( Font ) HeaderDefaults.Font.Clone();
                        this.Appearance.Justification = HeaderDefaults.Justification;
                        break;
                    }

                case HeadingType.Title:
                    {
                        this.Visible = TitleDefaults.Visible;
                        this.Appearance.Brush = ( Brush ) TitleDefaults.Brush.Clone();
                        this.Appearance.Font = ( Font ) TitleDefaults.Font.Clone();
                        this.Appearance.Justification = TitleDefaults.Justification;
                        break;
                    }

                case HeadingType.Subtitle:
                    {
                        this.Visible = SubtitleDefaults.Visible;
                        this.Appearance.Brush = ( Brush ) SubtitleDefaults.Brush.Clone();
                        this.Appearance.Font = ( Font ) SubtitleDefaults.Font.Clone();
                        this.Appearance.Justification = SubtitleDefaults.Justification;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "unhandled heading type" );
                        break;
                    }
            }
        }

        /// <summary>
        /// Default constructor for <see cref="ReportDocument">report</see> Title sets all title
        /// properties to default values as defined in the <see cref="HeaderDefaults" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="caption"> The Heading caption. </param>
        /// <param name="type">    The heading <see cref="HeadingType">type</see> </param>
        public Heading( string caption, HeadingType type ) : this( type )
        {
            if ( string.IsNullOrWhiteSpace( caption ) )
            {
                caption = string.Empty;
            }

            this.Appearance.Text = caption;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Title object from which to copy. </param>
        public Heading( Heading model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Appearance = model.Appearance.Copy();
            this.HeadingType = model.HeadingType;
            this.Visible = model.Visible;
            this.StatusMessage = model.StatusMessage;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status sentinel. </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( !(this.IsDisposed == true) )
                    {
                        if ( this.Appearance is object )
                        {
                            this.Appearance.Dispose();
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the Title. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of Title. </returns>
        public Heading Copy()
        {
            return new Heading( this );
        }

        /// <summary>
        /// Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
        /// of the title caption based on the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics" /> of the
        /// <see cref="M:Paint" /> method. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        public SizeF MeasureString( Graphics graphicsDevice )
        {
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : graphicsDevice.MeasureString( this.Appearance.Text, this.Appearance.Font );
        }

        /// <summary> Return the title caption. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A <see cref="System.String" /> value. </returns>
        public override string ToString()
        {
            return this.Appearance.Text;
        }

        /// <summary>
        /// Writes the <see cref="Heading">Heading</see> using the <see cref="Graphics" /> device
        /// specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        /// <returns> The line height. </returns>
        public int Write( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            var headerAppearance = this.Appearance.Copy();
            if ( this.IsDate )
            {
                headerAppearance.Text = this.Appearance.Text.Length > 0 ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.Appearance.Text, DateTime.Now.ToShortDateString() ) : DateTime.Now.ToShortDateString();
            }
            else if ( this.IsPage )
            {
                headerAppearance.Text = this.IsPageCount ? this.Appearance.Text.Length > 0 ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.Appearance.Text, e.PageNumber, e.PageCount ) : $"Page {e.PageNumber} of {e.PageCount}" : this.Appearance.Text.Length > 0 ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.Appearance.Text, e.PageNumber ) : $"Page {e.PageNumber}";
            }

            return headerAppearance.Write( e );
        }

        /// <summary> Returns the heading line height. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> References to the graphics device. </param>
        /// <returns> System.Int32. </returns>
        public int LineHeight( Graphics graphicsDevice )
        {
            // validate argument.
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : Convert.ToInt32( this.Appearance.Font.GetHeight( graphicsDevice ) );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the text appearance for the heading. </summary>
        /// <value> A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> </value>
        public TextAppearance Appearance { get; set; }

        /// <summary> Determines if the <see cref="Heading" /> will be show the date. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsDate { get; set; }

        /// <summary> Determines if the <see cref="Heading" /> will show the page. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsPage { get; set; }

        /// <summary>
        /// Determines if the <see cref="Heading" /> will show the page count as page x of N.
        /// </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsPageCount { get; set; }

        /// <summary> Determines if the <see cref="Heading" /> will be drawn. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="HeadingType">Heading type</see> for the
        /// <see cref="Heading" />.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public HeadingType HeadingType { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Heading" /> class that defines the default property
    /// values for the <see cref="Heading" /> footer.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class FooterDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private FooterDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the heading footer. This defaults to a solid dark gray brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.DarkGray;

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Heading" /> footer.
        /// </summary>
        /// <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
        public static bool Visible { get; set; } = true;

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 10f, FontStyle.Regular );

        /// <summary> Gets or sets the justification. </summary>
        /// <value> The justification. </value>
        public static LineJustification Justification { get; set; } = LineJustification.Left;
    }

    /// <summary>
    /// A simple subclass of the <see cref="Heading" /> class that defines the default property
    /// values for the <see cref="Heading" /> header.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class HeaderDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private HeaderDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the heading header. This defaults to a solid dark gray brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.DarkGray;

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Heading" />
        /// header.
        /// </summary>
        /// <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
        public static bool Visible { get; set; } = true;

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 10f, FontStyle.Regular );

        /// <summary>
        /// Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
        /// of the heading header.
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public static LineJustification Justification { get; set; } = LineJustification.Left;
    }

    /// <summary>
    /// A simple subclass of the <see cref="Heading" /> class that defines the default property
    /// values for the <see cref="Heading" /> sub-title.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class SubtitleDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private SubtitleDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the heading sub-title. This defaults to a solid black brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.Black;

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Heading" /> sub-title.
        /// </summary>
        /// <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
        public static bool Visible { get; set; } = true;

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 12f, FontStyle.Regular | FontStyle.Bold );

        /// <summary>
        /// Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
        /// of the heading sub-title.
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public static LineJustification Justification { get; set; } = LineJustification.Centered;
    }

    /// <summary>
    /// A simple subclass of the <see cref="Heading" /> class that defines the default property
    /// values for the <see cref="Heading" /> title.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class TitleDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private TitleDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the heading title. This defaults to a solid black brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.Black;

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Heading" />
        /// title.
        /// </summary>
        /// <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
        public static bool Visible { get; set; } = true;

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 16f, FontStyle.Regular | FontStyle.Bold );

        /// <summary>
        /// Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
        /// of the heading title.
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public static LineJustification Justification { get; set; } = LineJustification.Centered;
    }

    #endregion

    #region " TYPES "

    /// <summary> Enumerates the type of report headings. </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public enum HeadingType
    {

        /// <summary>
        /// A title to print on the first page
        /// </summary>
        [Description( "Title" )]
        Title,

        /// <summary>
        /// A sub-title to print on the first page
        /// </summary>
        [Description( "Subtitle" )]
        Subtitle,

        /// <summary>
        /// A header to print at the top of each page
        /// </summary>
        [Description( "Header" )]
        Header,

        /// <summary>
        /// A sub-header to print at top each page below the header
        /// </summary>
        [Description( "SubHeader" )]
        SubHeader,

        /// <summary>
        /// A super header to print at top each page above the header
        /// </summary>
        [Description( "SupHeader" )]
        SupHeader,

        /// <summary>
        /// A footer to print at bottom of each page
        /// </summary>
        [Description( "Footer" )]
        Footer,

        /// <summary>
        /// A sub-footer to print at bottom of each page below the footer
        /// </summary>
        [Description( "SubFooter" )]
        SubFooter,

        /// <summary>
        /// A super footer to print at bottom of each page above the footer
        /// </summary>
        [Description( "SupFooter" )]
        SupFooter
    }
}

#endregion
