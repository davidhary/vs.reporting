using System;

namespace isr.Visuals.Reporting
{

    /// <summary> Contains a list of <see cref="Heading" /> objects to print in the report. </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed class HeadingCollection : System.Collections.ObjectModel.Collection<Heading>, ICloneable
    {

        /// <summary>
        /// Default constructor for the <see cref="HeadingCollection">Heading Collection</see> class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public HeadingCollection() : base()
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The <see cref="HeadingCollection">Heading Collection</see>
        /// from which to copy. </param>
        public HeadingCollection( HeadingCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( var item in model )
                this.Add( new Heading( item ) );
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns>
        /// A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see>
        /// </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns>
        /// A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see>
        /// </returns>
        public HeadingCollection Copy()
        {
            return new HeadingCollection( this );
        }

        /// <summary>
        /// Writes the <see cref="Heading">Heading</see> using the <see cref="System.Drawing.Graphics" /> device
        /// specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e">           Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        /// <param name="headingType"> The <see cref="HeadingType">Heading type</see> for the
        /// <see cref="Heading" />. </param>
        /// <returns> The line height of the heading. </returns>
        public int Write( ReportPageEventArgs e, HeadingType headingType )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            int lineHeight = 0;
            foreach ( Heading Heading in this )
            {
                if ( Heading.Visible )
                {
                    if ( Heading.HeadingType == headingType )
                    {
                        int i = Heading.Write( e );
                        if ( i > lineHeight )
                        {
                            lineHeight = i;
                        }
                    }
                }
            }

            return lineHeight;
        }
    }
}
