using System;
using System.Drawing;

namespace isr.Visuals.Reporting
{

    /// <summary>
    /// A class that represents a text object on the graph.  A list of
    /// <see cref="TextBox" /> objects is maintained by the <see cref="TextBoxCollection" />
    /// collection class.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class TextBox : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="TextBox" /> with properties from the
        /// <see cref="CaptionDefaults" /> and <see cref="BodyTextDefaults" /> classes.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="area"> The layout area <see cref="RectangleF">Rectangle</see> </param>
        public TextBox( RectangleF area ) : base()
        {
            this.PageNumber = 1;
            this.Caption = new TextAppearance( string.Empty, new Font( CaptionDefaults.Font, CaptionDefaults.Font.Style ), ( Brush ) CaptionDefaults.Brush.Clone(), CaptionDefaults.Justification );
            this.BodyText = new TextAppearance( string.Empty, new Font( BodyTextDefaults.Font, BodyTextDefaults.Font.Style ), ( Brush ) BodyTextDefaults.Brush.Clone(), BodyTextDefaults.Justification );
            this.Layout = new LayoutArea( area );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The isr.Visuals.Reporting.TextBox object from which to copy. </param>
        public TextBox( TextBox model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.BodyText = model.BodyText.Copy();
            this.Caption = model.Caption.Copy();
            this.Layout = model.Layout.Copy();
            this.StatusMessage = model.StatusMessage;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status sentinel. </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( !this.IsDisposed )
                    {

                        // Free managed resources when explicitly called
                        if ( this.BodyText is object )
                        {
                            this.BodyText.Dispose();
                            this.BodyText = null;
                        }

                        if ( this.Caption is object )
                        {
                            this.Caption.Dispose();
                            this.Caption = null;
                        }

                        if ( this.Layout is object )
                        {
                            this.Layout.Dispose();
                            this.Layout = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the isr.Visuals.Reporting.TextBox. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the isr.Visuals.Reporting.TextBox. </returns>
        public TextBox Copy()
        {
            return new TextBox( this );
        }

        /// <summary>
        /// Writes the caption and body text into the <see cref="Graphics"/> device specified by the
        /// <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        /// <returns> The size of the text that was written. </returns>
        public SizeF Write( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            float textHeight = 0f;
            float textWidth = 0f;

            // exit if nothing to print
            if ( this.PageNumber != e.PageNumber )
            {
                return new SizeF( textWidth, textHeight );
            }

            e.CurrentX = Convert.ToInt32( this.Layout.Area.X );
            e.CurrentY = Convert.ToInt32( this.Layout.Area.Y );
            var captionSize = this.Caption.Write( e, this.Layout );
            // clone the body
            var body = this.BodyText.Copy();
            // set justification
            body.Justification = LineJustification.Left;
            // add spaces to indent by size of current x
            body.Text = string.Empty;
            var builder = new System.Text.StringBuilder();
            string space = string.Empty.PadRight( 1 );
            while ( body.MeasureString( e.Graphics ).Width <= captionSize.Width )
            {
                _ = builder.Append( space );
                body.Text = $"{builder}.";
            }

            _ = builder.Append( space );
            _ = builder.Append( space );
            _ = builder.Append( this.BodyText.Text );
            body.Text = builder.ToString();
            var bodyTextSize = body.Write( e, this.Layout );
            textHeight = bodyTextSize.Height;
            if ( captionSize.Height > textHeight )
            {
                textHeight = captionSize.Height;
            }

            textWidth = bodyTextSize.Width;
            if ( captionSize.Width > textWidth )
            {
                textWidth = captionSize.Width;
            }

            return new SizeF( textWidth, textHeight );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets a reference to the text box body of text. </summary>
        /// <value>
        /// A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> instance.
        /// </value>
        public TextAppearance BodyText { get; set; }

        /// <summary> Gets or sets a reference to the text box caption. </summary>
        /// <value>
        /// A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> instance.
        /// </value>
        public TextAppearance Caption { get; set; }

        /// <summary> The location of the <see cref="TextBox"/>. </summary>
        /// <value> The layout. </value>
        public LayoutArea Layout { get; set; }

        /// <summary> Gets or sets the text box page number. </summary>
        /// <value> The page number. </value>
        public int PageNumber { get; set; } = 1;

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="TextBox"/> class that defines the default property values
    /// for the <see cref="TextBox"/> caption.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class CaptionDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private CaptionDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the text box caption. This defaults to a solid black brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.Black;

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="TextBox"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 10f, FontStyle.Regular | FontStyle.Bold );

        /// <summary>
        /// Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
        /// of the TextBox caption.
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public static LineJustification Justification { get; set; } = LineJustification.Left;
    }

    /// <summary>
    /// A simple subclass of the <see cref="TextBox"/> class that defines the default property values
    /// for the <see cref="TextBox"/> body of text.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class BodyTextDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private BodyTextDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the text box body of text. This defaults to a solid black brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.Black;

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="TextBox"/> body of text.
        /// </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 10f, FontStyle.Regular );

        /// <summary>
        /// Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
        /// of the TextBox body of text.
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public static LineJustification Justification { get; set; } = LineJustification.None;
    }
}

#endregion
