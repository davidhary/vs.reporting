using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Visuals.Reporting
{

    /// <summary>
    /// <para>
    /// This class extends the functionality of the standard <see cref="T:System.Drawing.Printing.PrintDocument" />
    /// class by adding a number of properties and events that make report generation easier.
    /// Additionally, these events provide a <see cref="T:isr.Visuals.Reporting.ReportPageEventArgs" /> parameter
    /// which provides extra properties and methods beyond the normal
    /// <see cref="T:System.Drawing.Printing.PrintPageEventArgs" />, again simplifying the
    /// report generation process.
    /// </para><para>
    /// The ReportDocument class can be used just like a standard System.Drawing.Printing.PrintDocument
    /// class. In other words, the standard Print method, print dialogs and print preview capabilities
    /// of .NET work with ReportDocument just like they do with PrintDocument.
    /// </para>
    /// </summary>
    /// <remarks> (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// David, 2020-10-27. </para></remarks>
    [Description( "Report Document - Extends the Printing Print Document" )]
    [ToolboxBitmap( typeof( ReportDocument ) )]
    public class ReportDocument : System.Drawing.Printing.PrintDocument
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> This is the basic constructor method for this class. </summary>
        /// <remarks>
        /// This constructor sets the report to its default font of Courier New 10 points, black brush,
        /// and two footer lines.
        /// </remarks>
        public ReportDocument() : base()
        {
            base.QueryPageSettings += this.ReportDocument_QueryPageSettings;

            /// <summary> Handles the BeginPrint event of the reportDocument control. </summary>
            /// <remarks> David, 2020-10-27. </remarks>
            /// <param name="sender"> The source of the event. </param>
            /// <param name="e">      The <see cref="System.Drawing.Printing.PrintEventArgs" /> instance
            /// containing the event data. </param>
            base.BeginPrint += this.ReportDocument_BeginPrint;

            /// <summary> Handles the PrintPage event of the reportDocument control. </summary>
            /// <remarks> David, 2020-10-27. </remarks>
            /// <param name="sender"> The source of the event. </param>
            /// <param name="e">      The <see cref="System.Drawing.Printing.PrintPageEventArgs" /> instance
            /// containing the event data. </param>
            base.PrintPage += this.ReportDocument_PrintPage;
            this.Font = ( Font ) ReportDocumentDefaults.Font.Clone();
            this.Brush = ( Brush ) ReportDocumentDefaults.Brush.Clone();
            this.FooterLines = ReportDocumentDefaults.FooterLines;
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks>
        /// This constructor sets the report to the requested font and brush and two footer lines.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="font">  is the report font. </param>
        /// <param name="brush"> is the report brush. </param>
        public ReportDocument( Font font, Brush brush ) : this()
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            if ( brush is null )
            {
                throw new ArgumentNullException( nameof( brush ) );
            }

            this.Font = font;
            this.Brush = brush;
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="font">        is the report font. </param>
        /// <param name="brush">       is the report brush. </param>
        /// <param name="footerLines"> is the number of footer lines. </param>
        public ReportDocument( Font font, Brush brush, int footerLines ) : this( font, brush )
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            if ( brush is null )
            {
                throw new ArgumentNullException( nameof( brush ) );
            }

            this.FooterLines = footerLines;
        }

        /// <summary> Gets the disposed status sentinel. </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Component" />
        /// and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( ReportStarted is object )
                    {
                        foreach ( Delegate d in ReportStarted.GetInvocationList() )
                            ReportStarted -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( ReportEnded is object )
                    {
                        foreach ( Delegate d in ReportEnded.GetInvocationList() )
                            ReportEnded -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( PrintPageStarted is object )
                    {
                        foreach ( Delegate d in PrintPageStarted.GetInvocationList() )
                            PrintPageStarted -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( PrintPageBodyStarted is object )
                    {
                        foreach ( Delegate d in PrintPageBodyStarted.GetInvocationList() )
                            PrintPageBodyStarted -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( PrintPageBodyEnded is object )
                    {
                        foreach ( Delegate d in PrintPageBodyEnded.GetInvocationList() )
                            PrintPageBodyEnded -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( PrintPageEnded is object )
                    {
                        foreach ( Delegate d in PrintPageEnded.GetInvocationList() )
                            PrintPageEnded -= ( EventHandler<ReportPageEventArgs> ) d;
                    }

                    if ( this.Font is object )
                    {
                        this.Font.Dispose();
                        this.Font = null;
                    }

                    if ( this.Brush is object )
                    {
                        this.Brush.Dispose();
                        this.Brush = null;
                    }

                    this.Columns?.Clear();
                    this.Columns = null;
                    this._DataSource = null;
                    this._DataMember = string.Empty;
                }
            }
            finally
            {
                this.IsDisposed = true;
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT DECLARATIONS "

        /// <summary>
        /// Raised once immediately before anything is printed to the report.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> ReportStarted;

        /// <summary>
        /// Raised for each page immediately before anything is printed to that
        /// page. The cursor is on the first line of the page.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> PrintPageStarted;

        /// <summary>
        /// Raised for each page immediately after the header for the page has
        /// been printed. The cursor is on the first line of the report body.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> PrintPageBodyStarted;

        /// <summary>
        /// Raised for each page immediately before the footer for the page
        /// is printed. The cursor is on the first line of the header.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> PrintPageBodyEnded;

        /// <summary>
        /// Raised for each page after the footer has been printed.
        /// The cursor is past the end of the footer, typically into the bottom margin of the page.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> PrintPageEnded;

        /// <summary>
        /// Raised once at the very end of the report after all other printing
        /// is complete. The cursor is past the end of the footer on the last page, typically
        /// into the bottom margin of the page.
        /// </summary>
        public event EventHandler<ReportPageEventArgs> ReportEnded;

        #endregion

        #region " METHODS "

        /// <summary> Shows the print dialog. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void ShowPrintDialog( object sender, EventArgs e )
        {
            using var dlg = new PrintDialog {
                Document = this
            };
            _ = dlg.ShowDialog();
        }

        /// <summary> Prints or previews the report. </summary>
        /// <remarks>
        /// http://social.MSDN.microsoft.com/Forums/en/VBGeneral/thread/4e6e60f8-55fe-4a14-848f-c8c1103864ff.
        /// </remarks>
        /// <param name="printPreview"> True to display print preview. </param>
        public void Print( bool printPreview )
        {
            if ( printPreview )
            {

                // display the report
                using var printSetupButton = new ToolStripButton( "Printer Setup" );
                printSetupButton.Click += this.ShowPrintDialog;
                using ( var dlg = new PrintPreviewDialog() )
                {
                    dlg.Document = this;
                    dlg.WindowState = FormWindowState.Normal;
                    _ = (( ToolStrip ) dlg.Controls[1]).Items.Add( printSetupButton );
                    _ = dlg.ShowDialog();
                }

                printSetupButton.Click -= this.ShowPrintDialog;
            }
            else
            {

                // if not print preview then just print
                this.Print();
            }
        }

        #endregion

        #region " Setup Methods "

        /// <summary>
        /// The Me._headings
        /// </summary>
        private HeadingCollection _Headings;

        /// <summary> Adds a heading to the report. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="caption"> The heading caption. </param>
        /// <param name="type">    The heading <see cref="isr.Visuals.Reporting.HeadingType">type</see> </param>
        /// <returns> A <see cref="isr.Visuals.Reporting.Heading">heading</see> </returns>
        public Heading AddHeading( string caption, HeadingType type )
        {
            if ( string.IsNullOrWhiteSpace( caption ) )
            {
                caption = string.Empty;
            }

            if ( this._Headings is null )
            {
                this._Headings = new HeadingCollection();
            }

            var item = new Heading( caption, type );
            this._Headings.Add( item );
            return item;
        }

        /// <summary>
        /// The Me._text boxes
        /// </summary>
        private TextBoxCollection _TextBoxes;

        /// <summary> Adds a text box to the report. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="area"> The text box area <see cref="RectangleF" /> </param>
        /// <returns> A <see cref="isr.Visuals.Reporting.TextBox">Text Box</see> </returns>
        public TextBox AddTextBox( RectangleF area )
        {
            if ( this._TextBoxes is null )
            {
                this._TextBoxes = new TextBoxCollection();
            }

            var item = new TextBox( area );
            this._TextBoxes.Add( item );
            return item;
        }

        #endregion

        #region " Properties and Settings "

        /// <summary>
        /// The Brush object that will be used to render the text of the report. This defaults to a solid
        /// black brush.
        /// </summary>
        /// <value> A Brush object. </value>
        public Brush Brush { get; set; }

        /// <summary>
        /// A collection of <see cref="T:isr.Visuals.Printing.ReportColumn" /> objects that define the
        /// columns to be displayed in a table if the report is bound to a data source via the
        /// <see cref="P:.Printing.Report.ReportDocument.DataSource" />
        /// property.
        /// </summary>
        /// <value> A collection of columns to be rendered in the report. </value>
        public ReportColumnCollection Columns { get; private set; } = new ReportColumnCollection();

        /// <summary>
        /// The Font object that will be used to generate the text of the report. This defaults to a 10
        /// point Courier New font.
        /// </summary>
        /// <value> A Font object. </value>
        public Font Font { get; set; }

        /// <summary>
        /// The number of lines reserved at the bottom of each page for the footer. This defaults to 2
        /// lines for the default footer. If you want to add extra lines to the footer you should
        /// increase this value accordingly.
        /// </summary>
        /// <value> The number of lines reserved for the page footer. </value>
        public int FooterLines { get; set; }

        /// <summary> Determines if the <see cref="ReportDocument" /> has a footer line. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsFooterLine { get; set; }

        /// <summary> Determines if the <see cref="ReportDocument" /> has header line. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsHeaderLine { get; set; }

        /// <summary>
        /// Gets or sets the number of pages.  This must be set by the report calling method.
        /// </summary>
        /// <value> The page count. </value>
        public int PageCount { get; set; }

        /// <summary>
        /// The Me._page number
        /// </summary>
        private int _PageNumber;

        /// <summary>
        /// The Me._row
        /// </summary>
        private int _Row;

        /// <summary> Gets or sets the report section number. </summary>
        /// <value> The section. </value>
        public int Section { get; set; }

        /// <summary>
        /// Gets or sets or gets the option to render the default footer at the bottom of each page.
        /// </summary>
        /// <value> A Boolean indicating whether the default footer should be suppressed. </value>
        public bool SuppressDefaultFooter { get; set; }

        /// <summary>
        /// Gets or sets or gets the option to render the default header at the top of each page.
        /// </summary>
        /// <value> A Boolean indicating whether the default header should be suppressed. </value>
        public bool SuppressDefaultHeader { get; set; }

        /// <summary> Gets or sets or gets the option to render the default text boxes. </summary>
        /// <value> A Boolean indicating whether the default text boxes should be suppressed. </value>
        public bool SuppressDefaultTextBoxes { get; set; }

        /// <summary>
        /// Gets or sets or gets the option to render the default title at the top of the first page.
        /// </summary>
        /// <value> A Boolean indicating whether the default title should be suppressed. </value>
        public bool SuppressDefaultTitle { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> <c>StatusMessage</c> is a String property. </value>
        public string StatusMessage { get; set; }

        #endregion

        #region " DataSource/DataMember "

        /// <summary> The data source. </summary>
        private object _DataSource;

        /// <summary>
        /// By setting this property we provide the report with a data source. The data in the data
        /// source will be rendered into the report in tabular format based on the columns defined in the
        /// <see cref="P:.Printing.Report.ReportDocument.Columns" />
        /// property.
        /// </summary>
        /// <value> A valid data source. </value>
        [Category( "Data" )]
        [RefreshProperties( RefreshProperties.Repaint )]
        [TypeConverter( "System.Windows.Forms.Design.DataSourceConverter, System.Design" )]
        public object DataSource
        {
            get => this._DataSource;

            set {
                this._DataSource = value;
                if ( this._AutoDiscover )
                    this.DoAutoDiscover();
                this._Row = 0;
            }
        }

        /// <summary>
        /// The Me._data member
        /// </summary>
        private string _DataMember;

        /// <summary>
        /// The DataMember property allows us to easily set a single column of data to be displayed when
        /// the report is bound to a data source. If we want to display multiple columns of data in the
        /// report we should use the <see cref="P:.Printing.Report.ReportDocument.Columns" />
        /// property to define the columns.
        /// </summary>
        /// <value> A valid data source. </value>
        [Category( "Data" )]
        [Editor( "System.Windows.Forms.Design.DataMemberListEditor, System.Design", typeof( System.Drawing.Design.UITypeEditor ) )]
        public string DataMember
        {
            get => this._DataMember;

            set {
                this._DataMember = value;
                if ( this._AutoDiscover )
                    this.DoAutoDiscover();
                this._Row = 0;
            }
        }

        /// <summary> Inners the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="dataSource"> The data source. </param>
        /// <param name="dataMember"> The data member. </param>
        /// <returns> IList. </returns>
        private static IList InnerDataSource( DataSet dataSource, string dataMember )
        {
            return string.IsNullOrWhiteSpace( dataMember ) ? (( IListSource ) dataSource.Tables[0]).GetList() : (( IListSource ) dataSource.Tables[dataMember]).GetList();
        }

        /// <summary> Inners the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="dataSource"> The data source. </param>
        /// <returns> IList. </returns>
        private static IList InnerDataSource( IListSource dataSource )
        {
            return dataSource.GetList();
        }

        /// <summary> Inners the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> IList. </returns>
        private IList InnerDataSource()
        {
            return this._DataSource is DataSet set
                ? InnerDataSource( set, this._DataMember )
                : this._DataSource is IListSource source ? InnerDataSource( source ) : ( IList ) this._DataSource;
        }

        #endregion

        #region " AutoDiscover "

        /// <summary>
        /// The Me._auto discover
        /// </summary>
        private bool _AutoDiscover;

        /// <summary>
        /// Automatically discovers all the columns on the data source and displays them in the report.
        /// </summary>
        /// <value> <c>True</c> if [auto discover]; otherwise, <c>False</c>. </value>
        [Category( "Data" )]
        public bool AutoDiscover
        {
            get => this._AutoDiscover;

            set {
                if ( this._AutoDiscover == false && value == true )
                {
                    this._AutoDiscover = value;
                    this.DoAutoDiscover();
                }
                else
                {
                    this._AutoDiscover = value;
                    if ( this._AutoDiscover == false )
                    {
                        this.Columns.Clear();
                    }
                }
            }
        }

        /// <summary> Discover report columns from the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void DoAutoDiscover()
        {
            var innerSource = this.InnerDataSource();
            this.Columns.Clear();
            if ( innerSource is object )
            {
                if ( !(innerSource is DataView dv) )
                {
                    this.DoAutoDiscover( innerSource );
                }
                else
                {
                    this.DoAutoDiscover( dv );
                }
            }
        }

        /// <summary> Discover report columns from the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when <paramref name="dataSource">data
        /// source</paramref> is nothing. </exception>
        /// <param name="dataSource"> The data source. </param>
        private void DoAutoDiscover( DataView dataSource )
        {
            if ( dataSource is null )
                throw new ArgumentNullException( nameof( dataSource ) );
            for ( int field = 0, loopTo = dataSource.Table.Columns.Count - 1; field <= loopTo; field++ )
                this.Columns.Add( new ReportColumn() {
                    Name = dataSource.Table.Columns[field].Caption,
                    Field = dataSource.Table.Columns[field].ColumnName
                } );
            this.Columns.SetEvenSpacing( 650 );
        }

        /// <summary> Discover report columns from the data source. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataSource"> The data source. </param>
        private void DoAutoDiscover( IList dataSource )
        {
            if ( dataSource is null )
                throw new ArgumentNullException( nameof( dataSource ) );
            if ( dataSource.Count > 0 )
            {
                // retrieve the first item from the list
                var obj = dataSource[0];
                if ( obj is ValueType && obj.GetType().IsPrimitive )
                {
                    // the value is a primitive value type
                    this.Columns.Add( new ReportColumn() { Name = "Value" } );
                }
                else if ( obj is string )
                {
                    // the value is a simple string
                    this.Columns.Add( new ReportColumn() { Name = "Text" } );
                }
                else
                {
                    // the value is an object or Structure
                    var sourceType = obj.GetType();
                    int column;

                    // retrieve a list of all public properties
                    var props = sourceType.GetProperties();
                    if ( props.GetUpperBound( 0 ) >= 0 )
                    {
                        var loopTo = props.GetUpperBound( 0 );
                        for ( column = 0; column <= loopTo; column++ )
                            this.Columns.Add( props[column].Name );
                    }

                    // retrieve a list of all public fields
                    var fields = sourceType.GetFields();
                    if ( fields.GetUpperBound( 0 ) >= 0 )
                    {
                        var loopTo1 = fields.GetUpperBound( 0 );
                        for ( column = 0; column <= loopTo1; column++ )
                            this.Columns.Add( fields[column].Name );
                    }

                    this.Columns.SetEvenSpacing( 650 );
                }
            }
        }

        #endregion

        #region " GetField "

        /// <summary> Gets a field from a data row view. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataRow">   The data row. </param>
        /// <param name="fieldName"> Name of the field. </param>
        /// <returns> System.String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static string GetField( DataRowView dataRow, string fieldName )
        {
            if ( string.IsNullOrWhiteSpace( fieldName ) )
                throw new ArgumentNullException( nameof( fieldName ) );
            if ( dataRow is null )
                throw new ArgumentNullException( nameof( dataRow ) );

            // this is a DataRowView from a DataView
            return dataRow[fieldName].ToString();
        }

        /// <summary> Gets a field from a data row view. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="obj">       The object. </param>
        /// <param name="fieldName"> Name of the field. </param>
        /// <returns> System.String. </returns>
        private static string GetField( object obj, string fieldName )
        {
            if ( string.IsNullOrWhiteSpace( fieldName ) )
            {
                throw new ArgumentNullException( nameof( fieldName ) );
            }

            if ( obj is DataRowView )
            {

                // this is a DataRowView from a DataView
                return GetField( obj, fieldName );
            }
            else if ( obj is ValueType && obj.GetType().IsPrimitive )
            {

                // this is a primitive value type
                return obj.ToString();
            }
            else if ( obj is string )
            {

                // this is a simple string
                return Convert.ToString( obj, System.Globalization.CultureInfo.CurrentCulture );
            }
            else
            {

                // this is an object or Structure
                var sourcetype = obj.GetType();

                // see if the field is a property
                var prop = sourcetype.GetProperty( fieldName );
                if ( prop is null || !prop.CanRead )
                {
                    // no readable property of that name exists - check for a field
                    var field = sourcetype.GetField( fieldName );
                    if ( field is null )
                    {
                        // no field exists either, return the field name
                        // as a debugging indicator
                        return "No such value " + fieldName;
                    }
                    else
                    {
                        // got a field, return its value
                        return field.GetValue( obj ).ToString();
                    }
                }
                else
                {
                    // found a property, return its value
                    return prop.GetValue( obj, null ).ToString();
                }
            }
        }

        #endregion

        #region " Do printing "

        /// <summary> Handles the QueryPageSettings event of the reportDocument control. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Drawing.Printing.QueryPageSettingsEventArgs" />
        /// instance containing the event data. </param>
        private void ReportDocument_QueryPageSettings( object sender, System.Drawing.Printing.QueryPageSettingsEventArgs e )
        {
        }

        private void ReportDocument_BeginPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            this._PageNumber = 0;
            this._Row = 0;
        }

        private void ReportDocument_PrintPage( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
            this._PageNumber += 1;

            // create our ReportPageEventArgs object for this page
            var page = new ReportPageEventArgs( e, this._PageNumber, this.Font, this.Brush, this.FooterLines ) { PageCount = PageCount };

            // if we're generating page 1 raise the ReportBegin event
            if ( this._PageNumber == 1 )
            {
                var evt = ReportStarted;
                evt?.Invoke( this, page );
            }

            // set column widths if we have data bound columns
            if ( this.Columns.Count > 0 )
            {
                int space = Convert.ToInt32( e.MarginBounds.Width / ( double ) this.Columns.Count );
                int index;
                var loopTo = this.Columns.Count - 1;
                for ( index = 0; index <= loopTo; index++ )
                    this.Columns[index].Width = space;
            }

            // generate the page header/body/footer
            this.GeneratePage( page );

            // if there are no more pages to generate then raise
            // the ReportEnd event
            if ( !page.HasMorePages )
            {
                var evt = ReportEnded;
                evt?.Invoke( this, page );
            }

            // the client code may have overridden the Cancel or
            // HasMorePages values somewhere during the process,
            // so we restore them to the underlying PrintPageEventArgs
            // object - thus allowing our base class to take care
            // of these details for us
            e.Cancel = page.Cancel;
            e.HasMorePages = page.HasMorePages;
        }

        /// <summary> Generates the page. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
        private void GeneratePage( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            var innerSource = this.InnerDataSource();
            int field;

            // we're about to print the page
            var evt = PrintPageStarted;
            evt?.Invoke( this, e );

            // generate the header unless it is suppressed
            if ( !this.SuppressDefaultHeader )
            {
                this.PrintHeader( e );
            }

            // generate the title unless it is suppressed
            if ( !this.SuppressDefaultTitle )
            {
                this.PrintTitle( e );
            }
            else
            {
                e.PositionToStart();
            }

            if ( !this.SuppressDefaultTextBoxes )
            {
                // print the text boxes.
                this.PrintTextBoxes( e );
            }

            // we're about to print the body of the page
            evt = PrintPageBodyStarted;
            evt?.Invoke( this, e );

            // if we're data bound automatically generate the output
            // based on the data from the data source
            if ( this._DataSource is object && this.Columns.Count > 0 )
            {
                // load the data into the control
                while ( !e.EndOfPage() && this._Row < innerSource.Count )
                {
                    // load all subfields
                    var loopTo = this.Columns.Count - 1;
                    for ( field = 0; field <= loopTo; field++ )
                        e.WriteColumn( GetField( innerSource[this._Row], this.Columns[field].Field ).ToString(), this.Columns[field] );
                    e.WriteLine();
                    this._Row += 1;
                }

                e.HasMorePages = this._Row < innerSource.Count;
            }

            // we're done generating the body of this page
            evt = PrintPageBodyEnded;
            evt?.Invoke( this, e );

            // generate the page footer unless it is suppressed
            if ( !this.SuppressDefaultFooter )
            {
                this.PrintFooter( e );
            }

            // we're all done with the page
            evt = PrintPageEnded;
            evt?.Invoke( this, e );
        }

        /// <summary> Prints the header. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
        private void PrintHeader( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            // set vertical position to the top of the header region
            e.PositionToHeader();
            if ( this._Headings is object )
            {
                // write the super header
                e.WriteLine( this._Headings.Write( e, HeadingType.SupHeader ) );

                // write the header
                e.WriteLine( this._Headings.Write( e, HeadingType.Header ) );

                // write the sub-header
                e.WriteLine( this._Headings.Write( e, HeadingType.SubHeader ) );
            }

            // if we are data bound display column headers for the
            // data bound columns
            if ( this.Columns is object && this.Columns.Count > 0 )
            {
                // load the column headers
                for ( int field = 0, loopTo = this.Columns.Count - 1; field <= loopTo; field++ )
                    e.WriteColumn( this.Columns[field].Name, this.Columns[field] );
                e.WriteLine();
            }

            // display a horizontal line to separate the header from the body
            if ( this.IsHeaderLine )
            {
                e.HorizontalLine();
            }
        }

        /// <summary> Prints the footer. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
        private void PrintFooter( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( this._Headings is object )
            {

                // set vertical position to the top of the footer region
                e.PositionToFooter();

                // display a horizontal line to separate the body from the footer
                if ( this.IsFooterLine )
                {
                    e.HorizontalLine();
                }

                // write the super footers
                e.WriteLine( this._Headings.Write( e, HeadingType.SupFooter ) );

                // write the footers
                e.WriteLine( this._Headings.Write( e, HeadingType.Footer ) );

                // write the sub footers
                e.WriteLine( this._Headings.Write( e, HeadingType.SubFooter ) );
            }
        }

        /// <summary> Prints the title. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
        private void PrintTitle( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( this._Headings is object )
            {
                // set vertical position to the top of the page
                e.PositionToStart();

                // write the title
                e.WriteLine( this._Headings.Write( e, HeadingType.Title ) );

                // write the sub title
                e.WriteLine( this._Headings.Write( e, HeadingType.Subtitle ) );
            }
        }

        /// <summary> Prints the text boxes. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
        private void PrintTextBoxes( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( this._TextBoxes is object )
            {
                this._TextBoxes.Write( e );
            }
        }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="ReportDocument" /> class that defines the default
    /// property values for the <see cref="ReportDocument" />.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class ReportDocumentDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private ReportDocumentDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
        /// render the report body. This defaults to a solid dark gray brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public static Brush Brush { get; set; } = Brushes.DarkGray;

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public static Font Font { get; set; } = new Font( "Arial", 10f, FontStyle.Regular );

        /// <summary>
        /// Gets or sets the default footer lines for the <see cref="ReportDocument" /> body.
        /// </summary>
        /// <value> The footer lines. </value>
        public static int FooterLines { get; set; } = 2;
    }
}

#endregion
