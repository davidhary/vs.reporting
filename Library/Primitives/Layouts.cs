using System;
using System.Drawing;

namespace isr.Visuals.Reporting
{

    /// <summary> Renders Layout Areas for text. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class LayoutArea : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="LayoutArea" /> with default property values values as defined in the
        /// <see cref="LayoutDefaults" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="area"> The layout area <see cref="RectangleF" /> </param>
        public LayoutArea( RectangleF area ) : base()
        {
            this.Area = area;
            this.FillColor = LayoutDefaults.FillColor;
            this.Filled = LayoutDefaults.Filled;
            this.IsOutline = LayoutDefaults.IsOutline;
            this.Visible = LayoutDefaults.Visible;
            this.LineColor = LayoutDefaults.LineColor;
            this.LineWidth = LayoutDefaults.LineWidth;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The LayoutArea object from which to copy. </param>
        public LayoutArea( LayoutArea model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.StatusMessage = model.StatusMessage;
            this.Area = model.Area;
            this.FillColor = model.FillColor;
            this.Filled = model.Filled;
            this.IsOutline = model.IsOutline;
            this.Visible = model.Visible;
            this.LineColor = model.LineColor;
            this.LineWidth = model.LineWidth;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status sentinel. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( !(this.IsDisposed == true) )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the LayoutArea. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of LayoutArea. </returns>
        public LayoutArea Copy()
        {
            return new LayoutArea( this );
        }

        /// <summary> Render a <see cref="LayoutArea" />. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics" /> of the
        /// <see cref="M:Paint" /> method. </param>
        /// <param name="layoutArea">     The LayoutArea <see cref="RectangleF" />. </param>
        public void Draw( Graphics graphicsDevice, RectangleF layoutArea )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // If the background is to be filled, fill it
            if ( this.Filled )
            {
                using var fillBrush = new SolidBrush( this.FillColor );
                graphicsDevice.FillRectangle( fillBrush, Rectangle.Round( layoutArea ) );
            }

            // Draw the outline around 
            if ( this.IsOutline )
            {
                using var pen = new Pen( this.LineColor, this.LineWidth );
                graphicsDevice.DrawRectangle( pen, Rectangle.Round( layoutArea ) );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the layout area <see cref="RectangleF">Rectangle</see> </summary>
        /// <value> A <see cref="RectangleF" /> </value>
        public RectangleF Area { get; set; }

        /// <summary>
        /// Gets or sets the background color of the LayoutArea. Background fill is turned on or off
        /// using the <see cref="Filled" /> property.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
        public Color FillColor { get; set; }

        /// <summary>
        /// Gets or sets the fill mode of the <see cref="LayoutArea" />.  Set to True to fill the
        /// LayoutArea with color, or False otherwise.
        /// </summary>
        /// <value>
        /// True to fill the <see cref="LayoutArea" /> background with the <see cref="FillColor" />,
        /// False to leave the background transparent.
        /// </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the outline mode of the <see cref="LayoutArea" />.  Set to True to outline the
        /// LayoutArea with color, or False otherwise.
        /// </summary>
        /// <value> <c>True</c> if this instance is outline; otherwise, <c>False</c>. </value>
        public bool IsOutline { get; set; }

        /// <summary> Gets or sets a property that shows or hides the <see cref="LayoutArea" />. </summary>
        /// <value> True to show the symbol, False to hide it. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the pen width for drawing the <see cref="LayoutArea" /> outline.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the line color of the <see cref="LayoutArea" /> </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="LayoutArea" /> class that defines the default property
    /// values for the <see cref="LayoutArea" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed class LayoutDefaults
    {

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private LayoutDefaults()
        {
        }

        /// <summary>
        /// Gets or sets the default color for filling the LayoutArea
        /// (<see cref="LayoutArea.FillColor" /> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public static Color FillColor { get; set; } = Color.White;

        /// <summary>
        /// Gets or sets the default display mode for LayoutArea (<see cref="LayoutArea.Visible" />
        /// property). True to display the LayoutArea, False to hide the LayoutArea.
        /// </summary>
        /// <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
        public static bool Visible { get; set; } = true;

        /// <summary>
        /// Gets or sets the default pen width to be used for drawing LayoutArea outline
        /// (<see cref="LayoutArea.LineWidth" /> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public static float LineWidth { get; set; } = 1.0f;

        /// <summary>
        /// Gets or sets the default outline draw mode for LayoutArea
        /// (<see cref="LayoutArea.IsOutline" /> property). True to draw the LayoutArea outline, False
        /// otherwise.
        /// </summary>
        /// <value> <c>True</c> if this instance is outline; otherwise, <c>False</c>. </value>
        public static bool IsOutline { get; set; } = true;

        /// <summary>
        /// Gets or sets the default fill mode for the LayoutArea (<see cref="LayoutArea.Filled" />
        /// property). True to have LayoutArea filled in with color, False to leave LayoutArea as outline.
        /// </summary>
        /// <value> <c>True</c> if filled; otherwise, <c>False</c>. </value>
        public static bool Filled { get; set; } = true;

        /// <summary>
        /// Gets or sets the default color for drawing LayoutArea outline
        /// (<see cref="LayoutArea.LineColor" /> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public static Color LineColor { get; set; } = Color.Black;
    }
}

#endregion

