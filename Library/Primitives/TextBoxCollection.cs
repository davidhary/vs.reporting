using System;

namespace isr.Visuals.Reporting
{

    /// <summary> Contains a list of <see cref="TextBox" /> objects to display on the graph. </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed class TextBoxCollection : System.Collections.ObjectModel.Collection<TextBox>, ICloneable
    {

        /// <summary>
        /// Default constructor for the <see cref="TextBoxCollection"/> collection class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public TextBoxCollection() : base()
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The TextBoxCollection object from which to copy. </param>
        public TextBoxCollection( TextBoxCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( var item in model )
                this.Add( new TextBox( item ) );
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the TextBoxCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the TextBoxCollection. </returns>
        public TextBoxCollection Copy()
        {
            return new TextBoxCollection( this );
        }

        /// <summary>
        /// Writes the <see cref="TextBox">text box</see> using the <see cref="System.Drawing.Graphics"/> device
        /// specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        public void Write( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            foreach ( TextBox textBox in this )
                _ = textBox.Write( e );
        }
    }
}
