using System;
using System.Drawing;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Visuals.Reporting
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Handles specification and drawing of report Text. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class TextAppearance : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="ReportDocument">report</see>
        /// text appearance.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="text">          The text string. </param>
        /// <param name="font">          The font. </param>
        /// <param name="brush">         The brush for drawing the font. </param>
        /// <param name="justification"> The text justification. </param>
        public TextAppearance( string text, Font font, Brush brush, LineJustification justification ) : base()
        {
            if ( string.IsNullOrWhiteSpace( text ) )
                text = string.Empty;
            this.Font = font ?? new Font( "Arial", 10f, FontStyle.Regular );
            this.Brush = brush ?? Brushes.Black;
            this.Justification = justification == LineJustification.None ? LineJustification.Centered : justification;
            this.Text = string.IsNullOrWhiteSpace( text ) ? string.Empty : text;
            this.StatusMessage = string.Empty;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Title object from which to copy. </param>
        public TextAppearance( TextAppearance model ) : base()
        {
            if ( model is null )
                throw new ArgumentNullException( nameof( model ) );
            this.Text = model.Text;
            this.Font = ( Font ) model.Font.Clone();
            this.Brush = ( Brush ) model.Brush.Clone();
            this.Justification = model.Justification;
            this.StatusMessage = model.StatusMessage;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status sentinel. </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( !(this.IsDisposed == true) )
                    {

                        // Free managed resources when explicitly called
                        if ( this.Brush is object )
                        {
                            this.Brush.Dispose();
                        }

                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of the Title. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A new, independent copy of Title. </returns>
        public TextAppearance Copy()
        {
            return new TextAppearance( this );
        }

        /// <summary>
        /// Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
        /// of the title caption based on the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics" /> of the
        /// <see cref="M:Paint" /> method. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        public SizeF MeasureString( Graphics graphicsDevice )
        {
            return graphicsDevice is null ? throw new ArgumentNullException( nameof( graphicsDevice ) ) : graphicsDevice.MeasureString( this.Text, this.Font );
        }

        /// <summary>
        /// Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
        /// of the title caption based on the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics" /> of the
        /// <see cref="M:Paint" /> method. </param>
        /// <param name="layoutArea">     The layout area. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        public SizeF MeasureString( Graphics graphicsDevice, SizeF layoutArea )
        {
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : graphicsDevice.MeasureString( this.Text, this.Font, layoutArea );
        }

        /// <summary> Return the title caption. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A <see cref="System.String" /> value. </returns>
        public override string ToString()
        {
            return this.Text;
        }

        /// <summary>
        /// Writes the text using the <see cref="Graphics" /> device specified by the
        /// <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        /// <returns> The line height. </returns>
        public int Write( ReportPageEventArgs e )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( this.Text.Length > 0 )
            {
                e.Write( this.Text, this.Font, this.Brush, this.Justification );
                return Convert.ToInt32( this.Font.GetHeight( e.Graphics ) );
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Writes the text using the <see cref="Graphics" /> device specified by the
        /// <see cref="ReportPageEventArgs">report event arguments</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e">      Reference to the <see cref="ReportPageEventArgs">report event
        /// arguments</see>. </param>
        /// <param name="layout"> Reference to the <see cref="LayoutArea" /> </param>
        /// <returns> The size of the text that was written. </returns>
        public SizeF Write( ReportPageEventArgs e, LayoutArea layout )
        {

            // validate argument.
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( layout is null )
            {
                throw new ArgumentNullException( nameof( layout ) );
            }

            if ( this.Text.Length > 0 )
            {
                e.Write( this.Text, this.Font, this.Brush, this.Justification, layout.Area );
                return e.Graphics.MeasureString( this.Text, this.Font, layout.Area.Size );
            }
            else
            {
                return new SizeF( 0f, 0f );
            }
        }

        /// <summary> Returns the TextAppearance line height. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> References to the graphics device. </param>
        /// <returns> System.Int32. </returns>
        public int LineHeight( Graphics graphicsDevice )
        {
            // validate argument.
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : Convert.ToInt32( this.Font.GetHeight( graphicsDevice ) );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the <see cref="System.Drawing.Brush">Brush</see> that will be used to render the
        /// text of the TextAppearance. This defaults to a solid black brush.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
        public Brush Brush { get; set; }

        /// <summary>
        /// Gets or sets the text.  This text can be multiple lines, separated by new line characters.
        /// </summary>
        /// <value> A <see cref="System.String" /> property. </value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Text" /> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the TextAppearance <see cref="Reporting.LineJustification">justification</see>
        /// </summary>
        /// <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
        public LineJustification Justification { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        public string StatusMessage { get; set; }

        #endregion

    }
}
