## ISR Visuals Reporting Library<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*3.1.6667 04/03/18*  
2018 release.

*2.1.4767 01/18/13*  
Adds printer setup to the print preview dialog.

*2.1.4720 12/03/12*  
Updated to VS 2010.

*2.1.4232 08/03/11*  
Standardizes code elements and documentation.

*2.1.4213 07/15/11*  
Simplifies the assembly information.

*2.1.2961 02/09/08*  
Updated to .NET 3.5.

*2.0.2789 08/21/07*  
Updated to Visual Studio 8.

*1.0.2711 06/04/07*  
Limits rounding digits between 0 and 15.

*1.0.2228 02/06/06*  
Replaces Is Filled, Is Bold, etc. with Filled, Bold,
etc.

*1.0.2219 01/28/06*  
Split from the drawing library.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Reporting Library](https://bitbucket.org/davidhary/vs.reporting):
