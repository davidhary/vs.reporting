''' <summary> Contains a list of <see cref="Heading" /> objects to print in the report. </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class HeadingCollection
    Inherits System.Collections.ObjectModel.Collection(Of Heading)
    Implements ICloneable

    ''' <summary>
    ''' Default constructor for the <see cref="HeadingCollection">Heading Collection</see> class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The <see cref="HeadingCollection">Heading Collection</see>
    '''                      from which to copy. </param>
    Public Sub New(ByVal model As HeadingCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Dim item As isr.Visuals.Reporting.Heading
        For Each item In model
            Me.Add(New isr.Visuals.Reporting.Heading(item))
        Next item
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns>
    ''' A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see>
    ''' </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns>
    ''' A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see>
    ''' </returns>
    Public Function Copy() As HeadingCollection
        Return New HeadingCollection(Me)
    End Function

    ''' <summary>
    ''' Writes the <see cref="Heading">Heading</see> using the <see cref="Graphics" /> device
    ''' specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e">           Reference to the <see cref="ReportPageEventArgs">report event
    '''                            arguments</see>. </param>
    ''' <param name="headingType"> The <see cref="HeadingType">Heading type</see> for the
    '''                            <see cref="Heading" />. </param>
    ''' <returns> The line height of the heading. </returns>
    Public Function Write(ByVal e As ReportPageEventArgs, ByVal headingType As HeadingType) As Integer

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        Dim lineHeight As Integer = 0
        For Each Heading As Heading In Me
            If Heading.Visible Then
                If Heading.HeadingType = headingType Then
                    Dim i As Integer = Heading.Write(e)
                    If i > lineHeight Then
                        lineHeight = i
                    End If
                End If
            End If
        Next Heading
        Return lineHeight

    End Function

End Class
