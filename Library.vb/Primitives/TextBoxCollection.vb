''' <summary> Contains a list of <see cref="TextBox" /> objects to display on the graph. </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class TextBoxCollection
    Inherits System.Collections.ObjectModel.Collection(Of TextBox)
    Implements ICloneable

    ''' <summary>
    ''' Default constructor for the <see cref="TextBoxCollection"/> collection class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The TextBoxCollection object from which to copy. </param>
    Public Sub New(ByVal model As TextBoxCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Dim item As isr.Visuals.Reporting.TextBox
        For Each item In model
            Me.Add(New isr.Visuals.Reporting.TextBox(item))
        Next item
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the TextBoxCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the TextBoxCollection. </returns>
    Public Function Copy() As TextBoxCollection
        Return New TextBoxCollection(Me)
    End Function

    ''' <summary>
    ''' Writes the <see cref="TextBox">text box</see> using the <see cref="Graphics"/> device
    ''' specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
    '''                  arguments</see>. </param>
    Public Sub Write(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        For Each textBox As TextBox In Me
            textBox.Write(e)
        Next textBox

    End Sub

End Class
