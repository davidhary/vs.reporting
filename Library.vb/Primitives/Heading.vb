''' <summary> Handles specification and drawing of report headings. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Heading

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for <see cref="ReportDocument">report</see> title sets all title
    ''' properties to default values as defined in the <see cref="HeaderDefaults" /> class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="type"> The heading <see cref="HeadingType">type</see> </param>
    Public Sub New(ByVal type As HeadingType)

        MyBase.New()

        Me._Appearance = New TextAppearance(String.Empty,
                                         CType(TitleDefaults.Font.Clone, Font), CType(TitleDefaults.Brush.Clone, System.Drawing.Brush),
                                         TitleDefaults.Justification)
        Me._HeadingType = HeadingType.Title
        Me._Visible = TitleDefaults.Visible
        Me._StatusMessage = String.Empty

        Me._HeadingType = type
        Select Case Me._HeadingType

            Case HeadingType.Footer, HeadingType.SubFooter, HeadingType.SupFooter

                Me._Visible = FooterDefaults.Visible
                Me._Appearance.Brush = CType(FooterDefaults.Brush.Clone, System.Drawing.Brush)
                Me._Appearance.Font = CType(FooterDefaults.Font.Clone, Font)
                Me._Appearance.Justification = FooterDefaults.Justification

            Case HeadingType.Header, HeadingType.SubHeader, HeadingType.SupHeader

                Me._Visible = HeaderDefaults.Visible
                Me._Appearance.Brush = CType(HeaderDefaults.Brush.Clone, System.Drawing.Brush)
                Me._Appearance.Font = CType(HeaderDefaults.Font.Clone, Font)
                Me._Appearance.Justification = HeaderDefaults.Justification

            Case HeadingType.Title

                Me._Visible = TitleDefaults.Visible
                Me._Appearance.Brush = CType(TitleDefaults.Brush.Clone, System.Drawing.Brush)
                Me._Appearance.Font = CType(TitleDefaults.Font.Clone, Font)
                Me._Appearance.Justification = TitleDefaults.Justification

            Case HeadingType.Subtitle

                Me._Visible = SubtitleDefaults.Visible
                Me._Appearance.Brush = CType(SubtitleDefaults.Brush.Clone, System.Drawing.Brush)
                Me._Appearance.Font = CType(SubtitleDefaults.Font.Clone, Font)
                Me._Appearance.Justification = SubtitleDefaults.Justification

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "unhandled heading type")

        End Select

    End Sub

    ''' <summary>
    ''' Default constructor for <see cref="ReportDocument">report</see> Title sets all title
    ''' properties to default values as defined in the <see cref="HeaderDefaults" /> class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="caption"> The Heading caption. </param>
    ''' <param name="type">    The heading <see cref="HeadingType">type</see> </param>
    Public Sub New(ByVal caption As String, ByVal type As HeadingType)

        Me.New(type)
        If String.IsNullOrWhiteSpace(caption) Then
            caption = String.Empty
        End If
        Me._Appearance.Text = caption

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Title object from which to copy. </param>
    Public Sub New(ByVal model As Heading)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Appearance = model._Appearance.Copy
        Me._HeadingType = model._HeadingType
        Me._Visible = model._Visible
        Me._StatusMessage = model._StatusMessage

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the disposed status sentinel. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try


            If disposing Then

                If Not Me.IsDisposed = True Then

                    If Me._Appearance IsNot Nothing Then
                        Me._Appearance.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the Title. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of Title. </returns>
    Public Function Copy() As Heading
        Return New Heading(Me)
    End Function

    ''' <summary>
    ''' Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    ''' of the title caption based on the scaled font size.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics" /> of the
    '''                               <see cref="M:Paint" /> method. </param>
    ''' <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
    Public Function MeasureString(ByVal graphicsDevice As System.Drawing.Graphics) As SizeF
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        Return graphicsDevice.MeasureString(Me._Appearance.Text, Me._Appearance.Font)
    End Function

    ''' <summary> Return the title caption. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A <see cref="System.String" /> value. </returns>
    Public Overloads Overrides Function ToString() As String

        Return Me.Appearance.Text

    End Function

    ''' <summary>
    ''' Writes the <see cref="Heading">Heading</see> using the <see cref="Graphics" /> device
    ''' specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
    '''                  arguments</see>. </param>
    ''' <returns> The line height. </returns>
    Public Function Write(ByVal e As ReportPageEventArgs) As Integer

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        Dim headerAppearance As TextAppearance = Me._Appearance.Copy()
        If Me._IsDate Then
            headerAppearance.Text = If(Me.Appearance.Text.Length > 0,
                String.Format(Globalization.CultureInfo.CurrentCulture, Me._Appearance.Text, DateTime.Now.ToShortDateString),
                DateTime.Now.ToShortDateString())
        ElseIf Me._IsPage Then
            headerAppearance.Text = If(Me._IsPageCount,
                If(Me.Appearance.Text.Length > 0,
                    String.Format(Globalization.CultureInfo.CurrentCulture, Me._Appearance.Text, e.PageNumber, e.PageCount),
                    $"Page {e.PageNumber} of {e.PageCount}"),
                If(Me.Appearance.Text.Length > 0,
                    String.Format(Globalization.CultureInfo.CurrentCulture, Me._Appearance.Text, e.PageNumber),
                    $"Page {e.PageNumber}"))
        End If
        Return headerAppearance.Write(e)

    End Function

    ''' <summary> Returns the heading line height. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> References to the graphics device. </param>
    ''' <returns> System.Int32. </returns>
    Public Function LineHeight(ByVal graphicsDevice As Graphics) As Integer
        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        Return Convert.ToInt32(Me._Appearance.Font.GetHeight(graphicsDevice))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the text appearance for the heading. </summary>
    ''' <value> A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> </value>
    Public Property Appearance() As TextAppearance

    ''' <summary> Determines if the <see cref="Heading" /> will be show the date. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property IsDate() As Boolean

    ''' <summary> Determines if the <see cref="Heading" /> will show the page. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property IsPage() As Boolean

    ''' <summary>
    ''' Determines if the <see cref="Heading" /> will show the page count as page x of N.
    ''' </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property IsPageCount() As Boolean

    ''' <summary> Determines if the <see cref="Heading" /> will be drawn. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the <see cref="HeadingType">Heading type</see> for the
    ''' <see cref="Heading" />.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property HeadingType() As HeadingType

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the default property
''' values for the <see cref="Heading" /> footer.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class FooterDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the heading footer. This defaults to a solid dark gray brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.DarkGray

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" /> footer.
    ''' </summary>
    ''' <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
    Public Shared Property Visible() As Boolean = True

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 10, FontStyle.Regular)

    ''' <summary> Gets or sets the justification. </summary>
    ''' <value> The justification. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.Left

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the default property
''' values for the <see cref="Heading" /> header.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class HeaderDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the heading header. This defaults to a solid dark gray brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.DarkGray

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" />
    ''' header.
    ''' </summary>
    ''' <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
    Public Shared Property Visible() As Boolean = True

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 10, FontStyle.Regular)

    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading header.
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.Left

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the default property
''' values for the <see cref="Heading" /> sub-title.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class SubtitleDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the heading sub-title. This defaults to a solid black brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.Black

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" /> sub-title.
    ''' </summary>
    ''' <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
    Public Shared Property Visible() As Boolean = True

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 12, FontStyle.Regular Or FontStyle.Bold)

    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading sub-title.
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.Centered

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the default property
''' values for the <see cref="Heading" /> title.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class TitleDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the heading title. This defaults to a solid black brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.Black

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" />
    ''' title.
    ''' </summary>
    ''' <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
    Public Shared Property Visible() As Boolean = True

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 16, FontStyle.Regular Or FontStyle.Bold)

    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading title.
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.Centered

End Class

#End Region

#Region " TYPES "

''' <summary> Enumerates the type of report headings. </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public Enum HeadingType

    ''' <summary>
    ''' A title to print on the first page
    ''' </summary>
    <System.ComponentModel.Description("Title")> Title

    ''' <summary>
    ''' A sub-title to print on the first page
    ''' </summary>
    <System.ComponentModel.Description("Subtitle")> Subtitle

    ''' <summary>
    ''' A header to print at the top of each page
    ''' </summary>
    <System.ComponentModel.Description("Header")> Header

    ''' <summary>
    ''' A sub-header to print at top each page below the header
    ''' </summary>
    <System.ComponentModel.Description("SubHeader")> SubHeader

    ''' <summary>
    ''' A super header to print at top each page above the header
    ''' </summary>
    <System.ComponentModel.Description("SupHeader")> SupHeader

    ''' <summary>
    ''' A footer to print at bottom of each page
    ''' </summary>
    <System.ComponentModel.Description("Footer")> Footer

    ''' <summary>
    ''' A sub-footer to print at bottom of each page below the footer
    ''' </summary>
    <System.ComponentModel.Description("SubFooter")> SubFooter

    ''' <summary>
    ''' A super footer to print at bottom of each page above the footer
    ''' </summary>
    <System.ComponentModel.Description("SupFooter")> SupFooter

End Enum

#End Region
