''' <summary>
''' Defines a column into which text can be rendered on a line of a table when the
''' <see cref="T:.Printing.Report.ReportDocument" />
''' is bound to a data source.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class ReportColumn

    Implements IDisposable, ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks>
    ''' Use this constructor to instantiate this class and set the instance name, which is useful in
    ''' tracing.
    ''' </remarks>
    ''' <param name="instanceName"> Specifies the name of the instance. </param>
    Public Sub New(ByVal instanceName As String)

        ' instantiate the base class
        Me.New()
        Me._InstanceName = instanceName

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The ReportColumn object from which to copy. </param>
    Public Sub New(ByVal model As ReportColumn)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Field = model._Field
        Me._InstanceName = model._InstanceName
        Me._Left = model._Left
        Me._Name = model._Name
        Me._Width = model._Width
        Me._StatusMessage = model._StatusMessage

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the disposed status sentinel. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed = True Then

                    ' Free managed resources when explicitly called
                    Me._StatusMessage = String.Empty
                    Me._InstanceName = String.Empty

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Copy() As ReportColumn
        Return New ReportColumn(Me)
    End Function

    ''' <summary> Overrides ToString returning the instance name if not empty. </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, returns the base
    ''' class ToString value.
    ''' </remarks>
    ''' <returns> A <see cref="System.String" /> that represents this instance. </returns>
    Public Overrides Function ToString() As String
        Return If(String.IsNullOrWhiteSpace(Me._InstanceName), MyBase.ToString, Me._InstanceName)
    End Function

    ''' <summary>
    ''' The Me._instance name
    ''' </summary>
    Private _InstanceName As String = String.Empty

    ''' <summary> Gets or sets the name given to an instance of this class. </summary>
    ''' <value> <c>InstanceName</c> is a String property. </value>
    Public Property InstanceName() As String
        Get
            Return Me.ToString
        End Get
        Set(ByVal value As String)
            Me._InstanceName = value
        End Set
    End Property

    ''' <summary> Gets the status message. </summary>
    ''' <value> A <see cref="System.String">String</see>. </value>
    Public Property StatusMessage() As String

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Defines the human-readable name of the column. This value can be useful for generating
    ''' descriptive headers.
    ''' </summary>
    ''' <value> The name. </value>
    Public Property Name() As String

    ''' <summary>
    ''' Contains the name of the field within the data source that contains the data. This value is
    ''' used to retrieve the data value from the data source. It corresponds to the column name in a
    ''' DataTable, or a property name of an object.
    ''' </summary>
    ''' <value> The field. </value>
    Public Property Field() As String

    ''' <summary>
    ''' Defines the horizontal start location (X coordinate) of the column. When text is written to
    ''' the column by the
    ''' <see cref="M:isr.Visuals.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Visuals.Printing.ReportColumn)" />
    ''' method
    ''' it is rendered starting at this horizontal location.
    ''' </summary>
    ''' <value> The left. </value>
    Public Property Left() As Integer

    ''' <summary>
    ''' Defines the width of the column. Before text is written to the column by the
    ''' <see cref="M:isr.Visuals.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Visuals.Printing.ReportColumn)" />
    ''' method
    ''' the column is filled with a white rectangle defined by the width of the column. This helps
    ''' prevent text from overwriting other text within our columns.
    ''' </summary>
    ''' <value> The width. </value>
    Friend Property Width() As Integer

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

End Class

