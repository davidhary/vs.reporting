''' <summary>
''' A class that represents a text object on the graph.  A list of
''' <see cref="TextBox" /> objects is maintained by the <see cref="TextBoxCollection" />
''' collection class.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class TextBox
    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="TextBox" /> with properties from the
    ''' <see cref="CaptionDefaults" /> and <see cref="BodyTextDefaults" /> classes.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="area"> The layout area <see cref="RectangleF">Rectangle</see> </param>
    Public Sub New(ByVal area As RectangleF)

        MyBase.New()

        Me.PageNumber = 1
        Me.Caption = New TextAppearance(String.Empty, New Font(CaptionDefaults.Font, CaptionDefaults.Font.Style),
                                      CType(CaptionDefaults.Brush.Clone, Brush), CaptionDefaults.Justification)
        Me.BodyText = New TextAppearance(String.Empty, New Font(BodyTextDefaults.Font, BodyTextDefaults.Font.Style),
                                       CType(BodyTextDefaults.Brush.Clone, Brush), BodyTextDefaults.Justification)
        Me._Layout = New LayoutArea(area)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The isr.Visuals.Reporting.TextBox object from which to copy. </param>
    Public Sub New(ByVal model As isr.Visuals.Reporting.TextBox)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me.BodyText = model.BodyText.Copy
        Me.Caption = model.Caption.Copy
        Me._Layout = model._Layout.Copy
        Me.StatusMessage = model.StatusMessage

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the disposed status sentinel. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed Then

                    ' Free managed resources when explicitly called
                    If Me.BodyText IsNot Nothing Then
                        Me.BodyText.Dispose()
                        Me.BodyText = Nothing
                    End If
                    If Me.Caption IsNot Nothing Then
                        Me.Caption.Dispose()
                        Me.Caption = Nothing
                    End If
                    If Me._Layout IsNot Nothing Then
                        Me._Layout.Dispose()
                        Me._Layout = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the isr.Visuals.Reporting.TextBox. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the isr.Visuals.Reporting.TextBox. </returns>
    Public Function Copy() As isr.Visuals.Reporting.TextBox
        Return New isr.Visuals.Reporting.TextBox(Me)
    End Function

    ''' <summary>
    ''' Writes the caption and body text into the <see cref="Graphics"/> device specified by the
    ''' <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
    '''                  arguments</see>. </param>
    ''' <returns> The size of the text that was written. </returns>
    Public Function Write(ByVal e As ReportPageEventArgs) As SizeF

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        Dim textHeight As Single = 0
        Dim textWidth As Single = 0

        ' exit if nothing to print
        If Me.PageNumber <> e.PageNumber Then
            Return New SizeF(textWidth, textHeight)
        End If

        e.CurrentX = Convert.ToInt32(Me._Layout.Area.X)
        e.CurrentY = Convert.ToInt32(Me._Layout.Area.Y)
        Dim captionSize As SizeF = Me.Caption.Write(e, Me._Layout)
        ' clone the body
        Dim body As TextAppearance = Me.BodyText.Copy
        ' set justification
        body.Justification = LineJustification.Left
        ' add spaces to indent by size of current x
        body.Text = String.Empty
        Dim builder As New System.Text.StringBuilder
        Dim space As String = String.Empty.PadRight(1)
        Do Until body.MeasureString(e.Graphics).Width > captionSize.Width
            builder.Append(space)
            body.Text = $"{builder}."
        Loop
        builder.Append(space)
        builder.Append(space)
        builder.Append(Me.BodyText.Text)
        body.Text = builder.ToString
        Dim bodyTextSize As SizeF = body.Write(e, Me._Layout)
        textHeight = bodyTextSize.Height
        If captionSize.Height > textHeight Then
            textHeight = captionSize.Height
        End If
        textWidth = bodyTextSize.Width
        If captionSize.Width > textWidth Then
            textWidth = captionSize.Width
        End If
        Return New SizeF(textWidth, textHeight)

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets a reference to the text box body of text. </summary>
    ''' <value>
    ''' A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> instance.
    ''' </value>
    Public Property BodyText() As TextAppearance

    ''' <summary> Gets or sets a reference to the text box caption. </summary>
    ''' <value>
    ''' A <see cref="isr.Visuals.Reporting.TextAppearance">Text Appearance</see> instance.
    ''' </value>
    Public Property Caption() As TextAppearance

    ''' <summary> The location of the <see cref="TextBox"/>. </summary>
    ''' <value> The layout. </value>
    Public Property Layout() As isr.Visuals.Reporting.LayoutArea

    ''' <summary> Gets or sets the text box page number. </summary>
    ''' <value> The page number. </value>
    Public Property PageNumber() As Integer = 1

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="TextBox"/> class that defines the default property values
''' for the <see cref="TextBox"/> caption.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class CaptionDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the text box caption. This defaults to a solid black brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.Black

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="TextBox"/> caption.
    ''' </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 10, FontStyle.Regular Or FontStyle.Bold)

    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the TextBox caption.
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.Left

End Class

''' <summary>
''' A simple subclass of the <see cref="TextBox"/> class that defines the default property values
''' for the <see cref="TextBox"/> body of text.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class BodyTextDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the text box body of text. This defaults to a solid black brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.Black

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="TextBox"/> body of text.
    ''' </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 10, FontStyle.Regular)

    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the TextBox body of text.
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Shared Property Justification() As Reporting.LineJustification = LineJustification.None

End Class

#End Region
