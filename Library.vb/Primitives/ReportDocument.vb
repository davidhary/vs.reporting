''' <summary>
''' <para>
''' This class extends the functionality of the standard <see cref="T:System.Drawing.Printing.PrintDocument" />
''' class by adding a number of properties and events that make report generation easier.
''' Additionally, these events provide a <see cref="T:isr.Visuals.Reporting.ReportPageEventArgs" /> parameter
''' which provides extra properties and methods beyond the normal
'''   <see cref="T:System.Drawing.Printing.PrintPageEventArgs" />, again simplifying the
''' report generation process.
'''   </para><para>
''' The ReportDocument class can be used just like a standard System.Drawing.Printing.PrintDocument
''' class. In other words, the standard Print method, print dialogs and print preview capabilities
''' of .NET work with ReportDocument just like they do with PrintDocument.
'''   </para>
''' </summary>
''' <remarks> (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' David, 10/27/2020. </para></remarks>
<Description("Report Document - Extends the Printing Print Document"),
             System.Drawing.ToolboxBitmap(GetType(isr.Visuals.Reporting.ReportDocument))>
Public Class ReportDocument
    Inherits System.Drawing.Printing.PrintDocument

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> This is the basic constructor method for this class. </summary>
    ''' <remarks>
    ''' This constructor sets the report to its default font of Courier New 10 points, black brush,
    ''' and two footer lines.
    ''' </remarks>
    Public Sub New()

        MyBase.New()
        Me._Font = CType(ReportDocumentDefaults.Font.Clone, Font)
        Me._Brush = CType(ReportDocumentDefaults.Brush.Clone, Brush)
        Me._FooterLines = ReportDocumentDefaults.FooterLines

    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks>
    ''' This constructor sets the report to the requested font and brush and two footer lines.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="font">  is the report font. </param>
    ''' <param name="brush"> is the report brush. </param>
    Public Sub New(ByVal font As System.Drawing.Font, ByVal brush As System.Drawing.Brush)

        Me.New()
        If font Is Nothing Then
            Throw New ArgumentNullException(NameOf(font))
        End If
        If brush Is Nothing Then
            Throw New ArgumentNullException(NameOf(brush))
        End If

        Me._Font = font
        Me._Brush = brush

    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="font">        is the report font. </param>
    ''' <param name="brush">       is the report brush. </param>
    ''' <param name="footerLines"> is the number of footer lines. </param>
    Public Sub New(ByVal font As System.Drawing.Font, ByVal brush As System.Drawing.Brush, ByVal footerLines As Integer)

        Me.New(font, brush)
        If font Is Nothing Then
            Throw New ArgumentNullException(NameOf(font))
        End If
        If brush Is Nothing Then
            Throw New ArgumentNullException(NameOf(brush))
        End If
        Me._FooterLines = footerLines

    End Sub

    ''' <summary> Gets the disposed status sentinel. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Component" />
    ''' and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.ReportStartedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ReportStartedEvent.GetInvocationList
                        RemoveHandler Me.ReportStarted, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me.ReportEndedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ReportEndedEvent.GetInvocationList
                        RemoveHandler Me.ReportEnded, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me.PrintPageStartedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.PrintPageStartedEvent.GetInvocationList
                        RemoveHandler Me.PrintPageStarted, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me.PrintPageBodyStartedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.PrintPageBodyStartedEvent.GetInvocationList
                        RemoveHandler Me.PrintPageBodyStarted, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me.PrintPageBodyEndedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.PrintPageBodyEndedEvent.GetInvocationList
                        RemoveHandler Me.PrintPageBodyEnded, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me.PrintPageEndedEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.PrintPageEndedEvent.GetInvocationList
                        RemoveHandler Me.PrintPageEnded, CType(d, Global.System.EventHandler(Of ReportPageEventArgs))
                    Next
                End If
                If Me._Font IsNot Nothing Then Me._Font.Dispose() : Me._Font = Nothing
                If Me._Brush IsNot Nothing Then Me._Brush.Dispose() : Me._Brush = Nothing
                Me._Columns?.Clear() : Me._Columns = Nothing
                Me._DataSource = Nothing
                Me._DataMember = String.Empty
            End If
        Finally
            Me.IsDisposed = True
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT DECLARATIONS "

    ''' <summary>
    ''' Raised once immediately before anything is printed to the report.
    ''' </summary>
    Public Event ReportStarted As EventHandler(Of ReportPageEventArgs)

    ''' <summary>
    ''' Raised for each page immediately before anything is printed to that
    ''' page. The cursor is on the first line of the page.
    ''' </summary>
    Public Event PrintPageStarted As EventHandler(Of ReportPageEventArgs)

    ''' <summary>
    ''' Raised for each page immediately after the header for the page has
    ''' been printed. The cursor is on the first line of the report body.
    ''' </summary>
    Public Event PrintPageBodyStarted As EventHandler(Of ReportPageEventArgs)

    ''' <summary>
    ''' Raised for each page immediately before the footer for the page
    ''' is printed. The cursor is on the first line of the header.
    ''' </summary>
    Public Event PrintPageBodyEnded As EventHandler(Of ReportPageEventArgs)

    ''' <summary>
    ''' Raised for each page after the footer has been printed.
    ''' The cursor is past the end of the footer, typically into the bottom margin of the page.
    ''' </summary>
    Public Event PrintPageEnded As EventHandler(Of ReportPageEventArgs)

    ''' <summary>
    ''' Raised once at the very end of the report after all other printing
    ''' is complete. The cursor is past the end of the footer on the last page, typically
    ''' into the bottom margin of the page.
    ''' </summary>
    Public Event ReportEnded As EventHandler(Of ReportPageEventArgs)

#End Region

#Region " METHODS "

    ''' <summary> Shows the print dialog. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub ShowPrintDialog(ByVal sender As Object, ByVal e As EventArgs)
        Using dlg As New PrintDialog
            dlg.Document = Me
            dlg.ShowDialog()
        End Using
    End Sub

    ''' <summary> Prints or previews the report. </summary>
    ''' <remarks>
    ''' http://social.MSDN.microsoft.com/Forums/en/VBGeneral/thread/4e6e60f8-55fe-4a14-848f-c8c1103864ff.
    ''' </remarks>
    ''' <param name="printPreview"> True to display print preview. </param>
    Public Overloads Sub Print(ByVal printPreview As Boolean)

        If printPreview Then

            ' display the report
            Using printSetupButton As New ToolStripButton("Printer Setup")
                AddHandler printSetupButton.Click, AddressOf Me.ShowPrintDialog
                Using dlg As New System.Windows.Forms.PrintPreviewDialog
                    dlg.Document = Me
                    dlg.WindowState = System.Windows.Forms.FormWindowState.Normal
                    CType(dlg.Controls(1), ToolStrip).Items.Add(printSetupButton)
                    dlg.ShowDialog()
                End Using
                RemoveHandler printSetupButton.Click, AddressOf Me.ShowPrintDialog
            End Using

        Else

            ' if not print preview then just print
            MyBase.Print()

        End If

    End Sub

#End Region

#Region " Setup Methods "

    ''' <summary>
    ''' The Me._headings
    ''' </summary>
    Private _Headings As HeadingCollection

    ''' <summary> Adds a heading to the report. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="caption"> The heading caption. </param>
    ''' <param name="type">    The heading <see cref="isr.Visuals.Reporting.HeadingType">type</see> </param>
    ''' <returns> A <see cref="isr.Visuals.Reporting.Heading">heading</see> </returns>
    Public Function AddHeading(ByVal caption As String, ByVal type As HeadingType) As Heading
        If String.IsNullOrWhiteSpace(caption) Then
            caption = String.Empty
        End If
        If Me._Headings Is Nothing Then
            Me._Headings = New HeadingCollection
        End If
        Dim item As Heading = New Heading(caption, type)
        Me._Headings.Add(item)
        Return item
    End Function

    ''' <summary>
    ''' The Me._text boxes
    ''' </summary>
    Private _TextBoxes As TextBoxCollection

    ''' <summary> Adds a text box to the report. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="area"> The text box area <see cref="RectangleF" /> </param>
    ''' <returns> A <see cref="isr.Visuals.Reporting.TextBox">Text Box</see> </returns>
    Public Function AddTextBox(ByVal area As RectangleF) As TextBox
        If Me._TextBoxes Is Nothing Then
            Me._TextBoxes = New TextBoxCollection
        End If
        Dim item As TextBox = New TextBox(area)
        Me._TextBoxes.Add(item)
        Return item
    End Function

#End Region

#Region " Properties and Settings "

    ''' <summary>
    ''' The Brush object that will be used to render the text of the report. This defaults to a solid
    ''' black brush.
    ''' </summary>
    ''' <value> A Brush object. </value>
    Public Property Brush() As System.Drawing.Brush

    ''' <summary>
    ''' The Me._columns
    ''' </summary>
    Private _Columns As New ReportColumnCollection

    ''' <summary>
    ''' A collection of <see cref="T:isr.Visuals.Printing.ReportColumn" /> objects that define the
    ''' columns to be displayed in a table if the report is bound to a data source via the
    ''' <see cref="P:.Printing.Report.ReportDocument.DataSource" />
    ''' property.
    ''' </summary>
    ''' <value> A collection of columns to be rendered in the report. </value>
    Public ReadOnly Property Columns() As ReportColumnCollection
        Get
            Return Me._Columns
        End Get
    End Property

    ''' <summary>
    ''' The Font object that will be used to generate the text of the report. This defaults to a 10
    ''' point Courier New font.
    ''' </summary>
    ''' <value> A Font object. </value>
    Public Property Font() As System.Drawing.Font

    ''' <summary>
    ''' The number of lines reserved at the bottom of each page for the footer. This defaults to 2
    ''' lines for the default footer. If you want to add extra lines to the footer you should
    ''' increase this value accordingly.
    ''' </summary>
    ''' <value> The number of lines reserved for the page footer. </value>
    Public Property FooterLines() As Integer

    ''' <summary> Determines if the <see cref="ReportDocument" /> has a footer line. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property IsFooterLine() As Boolean

    ''' <summary> Determines if the <see cref="ReportDocument" /> has header line. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property IsHeaderLine() As Boolean

    ''' <summary>
    ''' Gets or sets the number of pages.  This must be set by the report calling method.
    ''' </summary>
    ''' <value> The page count. </value>
    Public Property PageCount() As Integer

    ''' <summary>
    ''' The Me._page number
    ''' </summary>
    Private _PageNumber As Integer

    ''' <summary>
    ''' The Me._row
    ''' </summary>
    Private _Row As Integer

    ''' <summary> Gets or sets the report section number. </summary>
    ''' <value> The section. </value>
    Public Property Section() As Integer

    ''' <summary>
    ''' Gets or sets or gets the option to render the default footer at the bottom of each page.
    ''' </summary>
    ''' <value> A Boolean indicating whether the default footer should be suppressed. </value>
    Public Property SuppressDefaultFooter() As Boolean

    ''' <summary>
    ''' Gets or sets or gets the option to render the default header at the top of each page.
    ''' </summary>
    ''' <value> A Boolean indicating whether the default header should be suppressed. </value>
    Public Property SuppressDefaultHeader() As Boolean

    ''' <summary> Gets or sets or gets the option to render the default text boxes. </summary>
    ''' <value> A Boolean indicating whether the default text boxes should be suppressed. </value>
    Public Property SuppressDefaultTextBoxes() As Boolean

    ''' <summary>
    ''' Gets or sets or gets the option to render the default title at the top of the first page.
    ''' </summary>
    ''' <value> A Boolean indicating whether the default title should be suppressed. </value>
    Public Property SuppressDefaultTitle() As Boolean

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> <c>StatusMessage</c> is a String property. </value>
    Public Property StatusMessage() As String

#End Region

#Region " DataSource/DataMember "

    ''' <summary> The data source. </summary>
    Private _DataSource As Object

    ''' <summary>
    ''' By setting this property we provide the report with a data source. The data in the data
    ''' source will be rendered into the report in tabular format based on the columns defined in the
    ''' <see cref="P:.Printing.Report.ReportDocument.Columns" />
    ''' property.
    ''' </summary>
    ''' <value> A valid data source. </value>
    <Category("Data"), RefreshProperties(RefreshProperties.Repaint),
    TypeConverter("System.Windows.Forms.Design.DataSourceConverter, System.Design")>
    Public Property DataSource() As Object
        Get
            Return Me._DataSource
        End Get
        Set(ByVal value As Object)
            Me._DataSource = value
            If Me._AutoDiscover Then Me.DoAutoDiscover()
            Me._Row = 0
        End Set
    End Property

    ''' <summary>
    ''' The Me._data member
    ''' </summary>
    Private _DataMember As String

    ''' <summary>
    ''' The DataMember property allows us to easily set a single column of data to be displayed when
    ''' the report is bound to a data source. If we want to display multiple columns of data in the
    ''' report we should use the <see cref="P:.Printing.Report.ReportDocument.Columns" />
    ''' property to define the columns.
    ''' </summary>
    ''' <value> A valid data source. </value>
    <Category("Data"), Editor("System.Windows.Forms.Design.DataMemberListEditor, System.Design", GetType(System.Drawing.Design.UITypeEditor))>
    Public Property DataMember() As String
        Get
            Return Me._DataMember
        End Get
        Set(ByVal value As String)
            Me._DataMember = value
            If Me._AutoDiscover Then Me.DoAutoDiscover()
            Me._Row = 0
        End Set
    End Property

    ''' <summary> Inners the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="dataSource"> The data source. </param>
    ''' <param name="dataMember"> The data member. </param>
    ''' <returns> IList. </returns>
    Private Shared Function InnerDataSource(ByVal dataSource As DataSet, ByVal dataMember As String) As IList

        Return If(String.IsNullOrWhiteSpace(dataMember),
            CType(dataSource.Tables(0), IListSource).GetList,
            CType(dataSource.Tables(dataMember), IListSource).GetList)

    End Function

    ''' <summary> Inners the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="dataSource"> The data source. </param>
    ''' <returns> IList. </returns>
    Private Shared Function InnerDataSource(ByVal dataSource As IListSource) As IList

        Return dataSource.GetList

    End Function

    ''' <summary> Inners the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> IList. </returns>
    Private Function InnerDataSource() As IList

        If TypeOf Me._DataSource Is DataSet Then

            Return ReportDocument.InnerDataSource(CType(Me._DataSource, DataSet), Me._DataMember)

        ElseIf TypeOf Me._DataSource Is IListSource Then

            Return ReportDocument.InnerDataSource(CType(Me._DataSource, IListSource))

        Else

            Return CType(Me._DataSource, IList)

        End If

    End Function

#End Region

#Region " AutoDiscover "

    ''' <summary>
    ''' The Me._auto discover
    ''' </summary>
    Private _AutoDiscover As Boolean

    ''' <summary>
    ''' Automatically discovers all the columns on the data source and displays them in the report.
    ''' </summary>
    ''' <value> <c>True</c> if [auto discover]; otherwise, <c>False</c>. </value>
    <Category("Data")>
    Public Property AutoDiscover() As Boolean
        Get
            Return Me._AutoDiscover
        End Get
        Set(ByVal value As Boolean)
            If Me._AutoDiscover = False AndAlso value = True Then
                Me._AutoDiscover = value
                Me.DoAutoDiscover()
            Else
                Me._AutoDiscover = value
                If Me._AutoDiscover = False Then
                    Me._Columns.Clear()
                End If
            End If
        End Set
    End Property

    ''' <summary> Discover report columns from the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub DoAutoDiscover()
        Dim innerSource As IList = Me.InnerDataSource()
        Me._Columns.Clear()
        If innerSource IsNot Nothing Then
            Dim dv As DataView = TryCast(innerSource, DataView)
            If dv Is Nothing Then
                Me.DoAutoDiscover(innerSource)
            Else
                Me.DoAutoDiscover(dv)
            End If
        End If
    End Sub

    ''' <summary> Discover report columns from the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when <paramref name="dataSource">data
    '''                                          source</paramref> is nothing. </exception>
    ''' <param name="dataSource"> The data source. </param>
    Private Sub DoAutoDiscover(ByVal dataSource As DataView)
        If dataSource Is Nothing Then Throw New ArgumentNullException(NameOf(dataSource))
        For field As Integer = 0 To dataSource.Table.Columns.Count - 1
            Me._Columns.Add(New ReportColumn With {
                            .Name = dataSource.Table.Columns(field).Caption,
                            .Field = dataSource.Table.Columns(field).ColumnName})
        Next
        Me._Columns.SetEvenSpacing(650)
    End Sub

    ''' <summary> Discover report columns from the data source. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataSource"> The data source. </param>
    Private Sub DoAutoDiscover(ByVal dataSource As IList)
        If dataSource Is Nothing Then Throw New ArgumentNullException(NameOf(dataSource))

        If dataSource.Count > 0 Then
            ' retrieve the first item from the list
            Dim obj As Object = dataSource.Item(0)

            If TypeOf obj Is ValueType AndAlso obj.GetType.IsPrimitive Then
                ' the value is a primitive value type
                Me._Columns.Add(New ReportColumn With {.Name = "Value"})

            ElseIf TypeOf obj Is String Then
                ' the value is a simple string
                Me._Columns.Add(New ReportColumn With {.Name = "Text"})
            Else
                ' the value is an object or Structure
                Dim sourceType As Type = obj.GetType
                Dim column As Integer

                ' retrieve a list of all public properties
                Dim props As System.Reflection.PropertyInfo() = sourceType.GetProperties()
                If props.GetUpperBound(0) >= 0 Then
                    For column = 0 To props.GetUpperBound(0)
                        Me._Columns.Add(props(column).Name)
                    Next
                End If

                ' retrieve a list of all public fields
                Dim fields As System.Reflection.FieldInfo() = sourceType.GetFields()
                If fields.GetUpperBound(0) >= 0 Then
                    For column = 0 To fields.GetUpperBound(0)
                        Me._Columns.Add(fields(column).Name)
                    Next
                End If

                Me._Columns.SetEvenSpacing(650)

            End If
        End If
    End Sub

#End Region

#Region " GetField "

    ''' <summary> Gets a field from a data row view. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataRow">   The data row. </param>
    ''' <param name="fieldName"> Name of the field. </param>
    ''' <returns> System.String. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function GetField(ByVal dataRow As DataRowView, ByVal fieldName As String) As String

        If String.IsNullOrWhiteSpace(fieldName) Then Throw New ArgumentNullException(NameOf(fieldName))
        If dataRow Is Nothing Then Throw New ArgumentNullException(NameOf(dataRow))

        ' this is a DataRowView from a DataView
        Return dataRow.Item(fieldName).ToString

    End Function

    ''' <summary> Gets a field from a data row view. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="obj">       The object. </param>
    ''' <param name="fieldName"> Name of the field. </param>
    ''' <returns> System.String. </returns>
    Private Shared Function GetField(ByVal obj As Object, ByVal fieldName As String) As String

        If String.IsNullOrWhiteSpace(fieldName) Then
            Throw New ArgumentNullException(NameOf(fieldName))
        End If

        If TypeOf obj Is DataRowView Then

            ' this is a DataRowView from a DataView
            Return GetField(obj, fieldName)

        ElseIf TypeOf obj Is ValueType AndAlso obj.GetType.IsPrimitive Then

            ' this is a primitive value type
            Return obj.ToString

        ElseIf TypeOf obj Is String Then

            ' this is a simple string
            Return Convert.ToString(obj, Globalization.CultureInfo.CurrentCulture)

        Else

            ' this is an object or Structure
            Dim sourcetype As Type = obj.GetType

            ' see if the field is a property
            Dim prop As System.Reflection.PropertyInfo = sourcetype.GetProperty(fieldName)

            If prop Is Nothing OrElse Not prop.CanRead Then
                ' no readable property of that name exists - check for a field
                Dim field As System.Reflection.FieldInfo = sourcetype.GetField(fieldName)

                If field Is Nothing Then
                    ' no field exists either, return the field name
                    ' as a debugging indicator
                    Return "No such value " & fieldName

                Else
                    ' got a field, return its value
                    Return field.GetValue(obj).ToString
                End If

            Else
                ' found a property, return its value
                Return prop.GetValue(obj, Nothing).ToString
            End If

        End If

    End Function

#End Region

#Region " Do printing "

    ''' <summary> Handles the QueryPageSettings event of the reportDocument control. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Drawing.Printing.QueryPageSettingsEventArgs" />
    '''                       instance containing the event data. </param>
    Private Sub ReportDocument_QueryPageSettings(ByVal sender As Object,
                                                 ByVal e As System.Drawing.Printing.QueryPageSettingsEventArgs) Handles MyBase.QueryPageSettings

    End Sub

    ''' <summary> Handles the BeginPrint event of the reportDocument control. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Drawing.Printing.PrintEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub ReportDocument_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles MyBase.BeginPrint

        Me._PageNumber = 0
        Me._Row = 0

    End Sub

    ''' <summary> Handles the PrintPage event of the reportDocument control. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Drawing.Printing.PrintPageEventArgs" /> instance
    '''                       containing the event data. </param>
    Private Sub ReportDocument_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles MyBase.PrintPage

        Me._PageNumber += 1

        ' create our ReportPageEventArgs object for this page
        Dim page As New ReportPageEventArgs(e, Me._PageNumber, Me._Font, Me._Brush, Me._FooterLines) With {.PageCount = Me._PageCount}

        ' if we're generating page 1 raise the ReportBegin event
        If Me._PageNumber = 1 Then
            Dim evt As EventHandler(Of ReportPageEventArgs) = Me.ReportStartedEvent
            evt?.Invoke(Me, page)
        End If

        ' set column widths if we have data bound columns
        If Me._Columns.Count > 0 Then
            Dim space As Integer = Convert.ToInt32(e.MarginBounds.Width / Me._Columns.Count)
            Dim index As Integer
            For index = 0 To Me._Columns.Count - 1
                Me._Columns(index).Width = space
            Next
        End If

        ' generate the page header/body/footer
        Me.GeneratePage(page)

        ' if there are no more pages to generate then raise
        ' the ReportEnd event
        If Not page.HasMorePages Then
            Dim evt As EventHandler(Of ReportPageEventArgs) = Me.ReportEndedEvent
            evt?.Invoke(Me, page)
        End If

        ' the client code may have overridden the Cancel or
        ' HasMorePages values somewhere during the process,
        ' so we restore them to the underlying PrintPageEventArgs
        ' object - thus allowing our base class to take care
        ' of these details for us
        e.Cancel = page.Cancel
        e.HasMorePages = page.HasMorePages

    End Sub

    ''' <summary> Generates the page. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
    Private Sub GeneratePage(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        Dim innerSource As IList = Me.InnerDataSource()
        Dim field As Integer

        ' we're about to print the page
        Dim evt As EventHandler(Of ReportPageEventArgs) = Me.PrintPageStartedEvent
        evt?.Invoke(Me, e)

        ' generate the header unless it is suppressed
        If Not Me.SuppressDefaultHeader Then
            Me.PrintHeader(e)
        End If

        ' generate the title unless it is suppressed
        If Not Me._SuppressDefaultTitle Then
            Me.PrintTitle(e)
        Else
            e.PositionToStart()
        End If

        If Not Me._SuppressDefaultTextBoxes Then
            ' print the text boxes.
            Me.PrintTextBoxes(e)
        End If

        ' we're about to print the body of the page
        evt = Me.PrintPageBodyStartedEvent
        evt?.Invoke(Me, e)

        ' if we're data bound automatically generate the output
        ' based on the data from the data source
        If Me._DataSource IsNot Nothing AndAlso Me._Columns.Count > 0 Then
            ' load the data into the control
            While Not e.EndOfPage AndAlso Me._Row < innerSource.Count
                ' load all subfields
                For field = 0 To Me._Columns.Count - 1
                    e.WriteColumn(GetField(innerSource.Item(Me._Row), Me._Columns(field).Field).ToString, Me._Columns(field))
                Next
                e.WriteLine()
                Me._Row += 1
            End While

            e.HasMorePages = Me._Row < innerSource.Count
        End If

        ' we're done generating the body of this page
        evt = Me.PrintPageBodyEndedEvent
        evt?.Invoke(Me, e)

        ' generate the page footer unless it is suppressed
        If Not Me._SuppressDefaultFooter Then
            Me.PrintFooter(e)
        End If

        ' we're all done with the page
        evt = Me.PrintPageEndedEvent
        evt?.Invoke(Me, e)

    End Sub

    ''' <summary> Prints the header. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
    Private Sub PrintHeader(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        ' set vertical position to the top of the header region
        e.PositionToHeader()

        If Me._Headings IsNot Nothing Then
            ' write the super header
            e.WriteLine(Me._Headings.Write(e, HeadingType.SupHeader))

            ' write the header
            e.WriteLine(Me._Headings.Write(e, HeadingType.Header))

            ' write the sub-header
            e.WriteLine(Me._Headings.Write(e, HeadingType.SubHeader))
        End If

        ' if we are data bound display column headers for the
        ' data bound columns
        If Me._Columns IsNot Nothing AndAlso (Me._Columns.Count > 0) Then
            ' load the column headers
            For field As Integer = 0 To Me._Columns.Count - 1
                e.WriteColumn(Me._Columns(field).Name, Me._Columns(field))
            Next
            e.WriteLine()
        End If

        ' display a horizontal line to separate the header from the body
        If Me._IsHeaderLine Then
            e.HorizontalLine()
        End If

    End Sub

    ''' <summary> Prints the footer. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
    Private Sub PrintFooter(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        If Me._Headings IsNot Nothing Then

            ' set vertical position to the top of the footer region
            e.PositionToFooter()

            ' display a horizontal line to separate the body from the footer
            If Me._IsFooterLine Then
                e.HorizontalLine()
            End If

            ' write the super footers
            e.WriteLine(Me._Headings.Write(e, HeadingType.SupFooter))

            ' write the footers
            e.WriteLine(Me._Headings.Write(e, HeadingType.Footer))

            ' write the sub footers
            e.WriteLine(Me._Headings.Write(e, HeadingType.SubFooter))

        End If

    End Sub

    ''' <summary> Prints the title. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
    Private Sub PrintTitle(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        If Me._Headings IsNot Nothing Then
            ' set vertical position to the top of the page
            e.PositionToStart()

            ' write the title
            e.WriteLine(Me._Headings.Write(e, HeadingType.Title))

            ' write the sub title
            e.WriteLine(Me._Headings.Write(e, HeadingType.Subtitle))
        End If

    End Sub

    ''' <summary> Prints the text boxes. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> The <see cref="ReportPageEventArgs" /> instance containing the event data. </param>
    Private Sub PrintTextBoxes(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        If Me._TextBoxes IsNot Nothing Then
            Me._TextBoxes.Write(e)
        End If

    End Sub

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="ReportDocument" /> class that defines the default
''' property values for the <see cref="ReportDocument" />.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class ReportDocumentDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be used to
    ''' render the report body. This defaults to a solid dark gray brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Shared Property Brush() As System.Drawing.Brush = System.Drawing.Brushes.DarkGray

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Shared Property Font() As Font = New Font("Arial", 10, FontStyle.Regular)

    ''' <summary>
    ''' Gets or sets the default footer lines for the <see cref="ReportDocument" /> body.
    ''' </summary>
    ''' <value> The footer lines. </value>
    Public Shared Property FooterLines() As Integer = 2

End Class

#End Region
