''' <summary>
''' Defines a strongly-typed collection that contains
''' <see cref="T:isr.Visuals.Printing.ReportColumn" /> objects.
''' </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class ReportColumnCollection
    Inherits System.Collections.ObjectModel.Collection(Of ReportColumn)
    Implements ICloneable

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Default constructor for the collection class. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The ReportColumnCollection object from which to copy. </param>
    Public Sub New(ByVal model As ReportColumnCollection)
        MyBase.New()
        If model Is Nothing Then Throw New ArgumentNullException(NameOf(model))
        Dim item As ReportColumn
        For Each item In model
            MyBase.Add(New ReportColumn(item))
        Next item
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the ArrowCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the ArrowCollection. </returns>
    Public Function Copy() As ReportColumnCollection
        Return New ReportColumnCollection(Me)
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
    ''' field name. The Name and Field of the column are set to the provided field name. The Left and
    ''' Width values are 0 and must be set separately.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="field"> The name of the data field. </param>
    Public Overloads Sub Add(ByVal field As String)
        If String.IsNullOrWhiteSpace(field) Then Throw New ArgumentNullException(NameOf(field))
        Me.Add(New ReportColumn With {
            .Name = field,
            .Field = field})
    End Sub

    ''' <summary>
    ''' Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
    ''' field name. The Name and Field of the column are set to the provided field name. The Left
    ''' value is set to the provided value. The Width value is 0 and must be set separately.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="field"> The name of the data field. </param>
    ''' <param name="left">  The X position of the column. </param>
    Public Overloads Sub Add(ByVal field As String, ByVal left As Integer)
        If String.IsNullOrWhiteSpace(field) Then Throw New ArgumentNullException(NameOf(field))
        Me.Add(New ReportColumn With {
            .Name = field,
            .Field = field,
            .Left = left})
    End Sub

    ''' <summary>
    ''' Adds a <see cref="T:isr.Visuals.Printing.ReportColumn" /> object to the collection based on a
    ''' field name. The Name and Field of the column are set to the provided values. The Left value
    ''' is set to the provided value. The Width value is 0 and must be set separately.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="name">  The human-readable column name. </param>
    ''' <param name="field"> The name of the data field. </param>
    ''' <param name="left">  The X position of the column. </param>
    Public Overloads Sub Add(ByVal name As String, ByVal field As String, ByVal left As Integer)
        If String.IsNullOrWhiteSpace(name) Then Throw New ArgumentNullException(NameOf(name))
        If String.IsNullOrWhiteSpace(field) Then Throw New ArgumentNullException(NameOf(field))
        Me.Add(New ReportColumn With {
            .Name = name,
            .Field = field,
            .Left = left})
    End Sub

    ''' <summary>
    ''' Called by the data binding mechanism to automatically run through all the columns defined by
    ''' this collection and to set their widths to evenly consume all the horizontal space on a line.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="width"> The total width of a printed line. </param>
    Friend Sub SetEvenSpacing(ByVal width As Integer)
        Dim space As Integer = Convert.ToInt32(width / MyBase.Count)
        Dim index As Integer
        For index = 0 To MyBase.Count - 1
            Dim col As ReportColumn = CType(MyBase.Item(index), ReportColumn)
            col.Left = space * index
            col.Width = space
        Next
    End Sub

#End Region

End Class
