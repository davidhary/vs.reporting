''' <summary> Renders Layout Areas for text. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class LayoutArea

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="LayoutArea" /> with default property values values as defined in the
    ''' <see cref="LayoutDefaults" /> class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="area"> The layout area <see cref="RectangleF" /> </param>
    Public Sub New(ByVal area As RectangleF)

        MyBase.New()

        Me._Area = area
        Me._FillColor = LayoutDefaults.FillColor
        Me._Filled = LayoutDefaults.Filled
        Me.IsOutline = LayoutDefaults.IsOutline
        Me._Visible = LayoutDefaults.Visible
        Me._LineColor = LayoutDefaults.LineColor
        Me._LineWidth = LayoutDefaults.LineWidth

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The LayoutArea object from which to copy. </param>
    Public Sub New(ByVal model As LayoutArea)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._StatusMessage = model.StatusMessage
        Me._Area = model._Area
        Me._FillColor = model._FillColor
        Me._Filled = model._Filled
        Me.IsOutline = model._IsOutline
        Me._Visible = model._Visible
        Me._LineColor = model._LineColor
        Me._LineWidth = model._LineWidth

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the disposed status sentinel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed = True Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the LayoutArea. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of LayoutArea. </returns>
    Public Function Copy() As LayoutArea
        Return New LayoutArea(Me)
    End Function

    ''' <summary> Render a <see cref="LayoutArea" />. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics" /> of the
    '''                               <see cref="M:Paint" /> method. </param>
    ''' <param name="layoutArea">     The LayoutArea <see cref="RectangleF" />. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal layoutArea As RectangleF)

        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' If the background is to be filled, fill it
        If Me._Filled Then
            Using fillBrush As New SolidBrush(Me._FillColor)
                graphicsDevice.FillRectangle(fillBrush, Rectangle.Round(layoutArea))
            End Using
        End If

        ' Draw the outline around 
        If Me._IsOutline Then
            Using pen As New Pen(Me._LineColor, Me._LineWidth)
                graphicsDevice.DrawRectangle(pen, Rectangle.Round(layoutArea))
            End Using
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the layout area <see cref="RectangleF">Rectangle</see> </summary>
    ''' <value> A <see cref="RectangleF" /> </value>
    Public Property Area() As RectangleF

    ''' <summary>
    ''' Gets or sets the background color of the LayoutArea. Background fill is turned on or off
    ''' using the <see cref="Filled" /> property.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
    Public Property FillColor() As Color

    ''' <summary>
    ''' Gets or sets the fill mode of the <see cref="LayoutArea" />.  Set to True to fill the
    ''' LayoutArea with color, or False otherwise.
    ''' </summary>
    ''' <value>
    ''' True to fill the <see cref="LayoutArea" /> background with the <see cref="FillColor" />,
    ''' False to leave the background transparent.
    ''' </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the outline mode of the <see cref="LayoutArea" />.  Set to True to outline the
    ''' LayoutArea with color, or False otherwise.
    ''' </summary>
    ''' <value> <c>True</c> if this instance is outline; otherwise, <c>False</c>. </value>
    Public Property IsOutline() As Boolean

    ''' <summary> Gets or sets a property that shows or hides the <see cref="LayoutArea" />. </summary>
    ''' <value> True to show the symbol, False to hide it. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the pen width for drawing the <see cref="LayoutArea" /> outline.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the line color of the <see cref="LayoutArea" /> </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="LayoutArea" /> class that defines the default property
''' values for the <see cref="LayoutArea" /> class.
''' </summary>
''' <remarks> David, 10/27/2020. </remarks>
Public NotInheritable Class LayoutDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The fill color
    ''' </summary>
    Private Shared _FillColor As Color = Color.White

    ''' <summary>
    ''' Gets or sets the default color for filling the LayoutArea
    ''' (<see cref="LayoutArea.FillColor" /> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Shared Property FillColor() As Color
        Get
            Return LayoutDefaults._FillColor
        End Get
        Set(ByVal value As Color)
            LayoutDefaults._FillColor = value
        End Set
    End Property

    Private Shared _Visible As Boolean = True

    ''' <summary>
    ''' Gets or sets the default display mode for LayoutArea (<see cref="LayoutArea.Visible" />
    ''' property). True to display the LayoutArea, False to hide the LayoutArea.
    ''' </summary>
    ''' <value> <c>True</c> if visible; otherwise, <c>False</c>. </value>
    Public Shared Property Visible() As Boolean
        Get
            Return LayoutDefaults._Visible
        End Get
        Set(ByVal value As Boolean)
            LayoutDefaults._Visible = value
        End Set
    End Property

    Private Shared _LineWidth As Single = 1.0F

    ''' <summary>
    ''' Gets or sets the default pen width to be used for drawing LayoutArea outline
    ''' (<see cref="LayoutArea.LineWidth" /> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Shared Property LineWidth() As Single
        Get
            Return LayoutDefaults._LineWidth
        End Get
        Set(ByVal value As Single)
            LayoutDefaults._LineWidth = value
        End Set
    End Property

    Private Shared _IsOutline As Boolean = True

    ''' <summary>
    ''' Gets or sets the default outline draw mode for LayoutArea
    ''' (<see cref="LayoutArea.IsOutline" /> property). True to draw the LayoutArea outline, False
    ''' otherwise.
    ''' </summary>
    ''' <value> <c>True</c> if this instance is outline; otherwise, <c>False</c>. </value>
    Public Shared Property IsOutline() As Boolean
        Get
            Return LayoutDefaults._IsOutline
        End Get
        Set(ByVal value As Boolean)
            LayoutDefaults._IsOutline = value
        End Set
    End Property

    Private Shared _Filled As Boolean = True

    ''' <summary>
    ''' Gets or sets the default fill mode for the LayoutArea (<see cref="LayoutArea.Filled" />
    ''' property). True to have LayoutArea filled in with color, False to leave LayoutArea as outline.
    ''' </summary>
    ''' <value> <c>True</c> if filled; otherwise, <c>False</c>. </value>
    Public Shared Property Filled() As Boolean
        Get
            Return LayoutDefaults._Filled
        End Get
        Set(ByVal value As Boolean)
            LayoutDefaults._Filled = value
        End Set
    End Property

    Private Shared _LineColor As Color = Color.Black

    ''' <summary>
    ''' Gets or sets the default color for drawing LayoutArea outline
    ''' (<see cref="LayoutArea.LineColor" /> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Shared Property LineColor() As Color
        Get
            Return LayoutDefaults._LineColor
        End Get
        Set(ByVal value As Color)
            LayoutDefaults._LineColor = value
        End Set
    End Property

End Class

#End Region

