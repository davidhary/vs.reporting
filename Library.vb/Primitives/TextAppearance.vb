''' <summary> Handles specification and drawing of report Text. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class TextAppearance

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for <see cref="ReportDocument">report</see>
    ''' text appearance.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="text">          The text string. </param>
    ''' <param name="font">          The font. </param>
    ''' <param name="brush">         The brush for drawing the font. </param>
    ''' <param name="justification"> The text justification. </param>
    Public Sub New(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal justification As LineJustification)

        MyBase.New()
        If String.IsNullOrWhiteSpace(text) Then text = String.Empty

        Me._Font = If(font, New Font("Arial", 10, FontStyle.Regular))

        Me._Brush = If(brush, System.Drawing.Brushes.Black)

        Me._Justification = If(justification = LineJustification.None, LineJustification.Centered, justification)

        Me._Text = If(String.IsNullOrWhiteSpace(text), String.Empty, text)

        Me._StatusMessage = String.Empty

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Title object from which to copy. </param>
    Public Sub New(ByVal model As TextAppearance)

        MyBase.New()
        If model Is Nothing Then Throw New ArgumentNullException(NameOf(model))
        Me._Text = model._Text
        Me._Font = CType(model._Font.Clone, Font)
        Me._Brush = CType(model._Brush.Clone, System.Drawing.Brush)
        Me._Justification = model._Justification
        Me._StatusMessage = model._StatusMessage

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the disposed status sentinel. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed = True Then

                    ' Free managed resources when explicitly called
                    If Me._Brush IsNot Nothing Then
                        Me._Brush.Dispose()
                    End If
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of the Title. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A new, independent copy of Title. </returns>
    Public Function Copy() As TextAppearance
        Return New TextAppearance(Me)
    End Function

    ''' <summary>
    ''' Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    ''' of the title caption based on the scaled font size.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics" /> of the
    '''                               <see cref="M:Paint" /> method. </param>
    ''' <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
    Public Function MeasureString(ByVal graphicsDevice As System.Drawing.Graphics) As SizeF
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        Return graphicsDevice.MeasureString(Me._Text, Me._Font)
    End Function

    ''' <summary>
    ''' Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    ''' of the title caption based on the scaled font size.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics" /> of the
    '''                               <see cref="M:Paint" /> method. </param>
    ''' <param name="layoutArea">     The layout area. </param>
    ''' <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
    Public Function MeasureString(ByVal graphicsDevice As System.Drawing.Graphics, ByVal layoutArea As SizeF) As SizeF
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        Return graphicsDevice.MeasureString(Me._Text, Me._Font, layoutArea)
    End Function

    ''' <summary> Return the title caption. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A <see cref="System.String" /> value. </returns>
    Public Overloads Overrides Function ToString() As String

        Return Me._Text

    End Function

    ''' <summary>
    ''' Writes the text using the <see cref="Graphics" /> device specified by the
    ''' <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Reference to the <see cref="ReportPageEventArgs">report event
    '''                  arguments</see>. </param>
    ''' <returns> The line height. </returns>
    Public Function Write(ByVal e As ReportPageEventArgs) As Integer

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If

        If Me._Text.Length > 0 Then
            e.Write(Me._Text, Me._Font, Me._Brush, Me._Justification)
            Return Convert.ToInt32(Me._Font.GetHeight(e.Graphics))
        Else
            Return 0
        End If

    End Function

    ''' <summary>
    ''' Writes the text using the <see cref="Graphics" /> device specified by the
    ''' <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e">      Reference to the <see cref="ReportPageEventArgs">report event
    '''                       arguments</see>. </param>
    ''' <param name="layout"> Reference to the <see cref="LayoutArea" /> </param>
    ''' <returns> The size of the text that was written. </returns>
    Public Function Write(ByVal e As ReportPageEventArgs, ByVal layout As LayoutArea) As SizeF

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException(NameOf(e))
        End If
        If layout Is Nothing Then
            Throw New ArgumentNullException(NameOf(layout))
        End If

        If Me._Text.Length > 0 Then
            e.Write(Me._Text, Me._Font, Me._Brush, Me._Justification, layout.Area)
            Return e.Graphics.MeasureString(Me._Text, Me._Font, layout.Area.Size)
        Else
            Return New SizeF(0, 0)
        End If

    End Function

    ''' <summary> Returns the TextAppearance line height. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> References to the graphics device. </param>
    ''' <returns> System.Int32. </returns>
    Public Function LineHeight(ByVal graphicsDevice As Graphics) As Integer
        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        Return Convert.ToInt32(Me._Font.GetHeight(graphicsDevice))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the <see cref="System.Drawing.Brush">Brush</see> that will be used to render the
    ''' text of the TextAppearance. This defaults to a solid black brush.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Brush">Brush</see> object. </value>
    Public Property Brush() As System.Drawing.Brush

    ''' <summary>
    ''' Gets or sets the text.  This text can be multiple lines, separated by new line characters.
    ''' </summary>
    ''' <value> A <see cref="System.String" /> property. </value>
    Public Property Text() As String

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Text" /> caption.
    ''' </summary>
    ''' <value> The font. </value>
    Public Property Font() As Font

    ''' <summary>
    ''' Gets or sets the TextAppearance <see cref="Reporting.LineJustification">justification</see>
    ''' </summary>
    ''' <value> A <see cref="Reporting.LineJustification">justification</see> value. </value>
    Public Property Justification() As Reporting.LineJustification

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    Public Property StatusMessage() As String

#End Region

End Class
