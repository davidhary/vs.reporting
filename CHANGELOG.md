# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.1.7162] - 2020-11-02
* Converted to C#.

## [3.1.6667] - 2018-04-03
* 2018 release.

## [2.1.4767] - 2013-01-18
* Adds printer setup to the print preview dialog.

## [2.1.4720] - 2012-02-03
* Updated to VS 2010.

## [2.1.4232] - 2011-08-03
* Standardizes code elements and documentation.

## [2.1.4213] - 2011-07-15
* Simplifies the assembly information.

## [2.1.2961] - 2008-02-09
* Updated to .NET 3.5.

## [2.0.2789] - 2007-08-21
* Updated to Visual Studio 8.

## [1.0.2711] - 2007-06-04
* Limits rounding digits between 0 and 15.

## [1.0.2228] - 2006-02-06
* Replaces Is Filled, Is Bold, etc. with Filled, Bold, etc.

## [1.0.2219] - 2006-01-28
* Split from the drawing library.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
